function [ alpha ] = alpha_dist(phi,psi,omega)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    
    [w,a] = meshgrid([-1,1],[-1,1]); % precompute partition sum
    Z = exp(phi*w + psi*a + w*a) + exp(-phi*w - psi*a - w*a);
    alpha = @(wp,w,a) = exp(phi*wp*w + psi*wp*a + omega*wp*w*a) ./  ..
                        Z((w + 1)/2 + 1,(a + 1)/2 + 1);
end

