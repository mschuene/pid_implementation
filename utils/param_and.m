d1 = param_dist(0.1,-0.05);

n = 10;
UIY = zeros(n,n);
UIZ = zeros(n,n);
SI = zeros(n,n);
CI = zeros(n,n);
idx = 1;jdx = 1;
for i=linspace(-1/8,1/8,n)
    jdx = 1;
    for j = linspace(-1/16,1/16,n)
        d2 = squeeze(sum(param_dist(i,j),1));
        [UIY(idx,jdx),UIZ(idx,jdx),SI(idx,jdx),CI(idx,jdx)] = PID(d2);
        jdx = jdx + 1;
    end
    idx = idx + 1
end
figure(2)
subplot(2,2,1);
surf(UIY);
subplot(2,2,2);
surf(UIZ);
subplot(2,2,3);
surf(SI);
subplot(2,2,4);
surf(CI);