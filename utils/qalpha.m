function [ qa ] = qalpha( alpha )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    qa = zeros(2,2,2);
    qa(0 + 1,0 + 1,0 + 1) = 0.25 + alpha;
    qa(0 + 1,0 + 1,1 + 1) = 0.25 - alpha;
    qa(0 + 1,1 + 1,0 + 1) = 0.25 - alpha;
    qa(0 + 1,1 + 1,1 + 1) =  alpha;
    qa(1 + 1,1 + 1,1 + 1) = 0.25;

end

