function [ decoded ] = decode_dist( encoded )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

    n = numel(encoded);
    decoded = zeros(n+1,1);
    decoded(n+1) = (1 - sum(encoded))/2;
    for i = 1:n
        decoded(i) = encoded(i) + decoded(n+1)/n;
    end
end

