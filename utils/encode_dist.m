function [ encoded ] = encode_dist(p)
% encode_dist encode distribution in cardx*cardy*cardz - 1 dimensional
% basis

    n = numel(p);
    p = p(:);
    encoded = zeros(n-1,1);
    for i = 1:(n-1)
        encoded(i) = p(i) - p(n)/(n - 1);
    end
end

