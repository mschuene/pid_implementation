function [ D ] = param_dist(b,deltam )
%PARAM_DIST Summary of this function goes here
%   Detailed explanation goes here

    D = zeros(2,3,2,2);
    a = 2*deltam; c = deltam + b/2;
    D(0+1,0+1,0+1,0+1) = 1/8 + a;
    D(0+1,1+1,0+1,0+1) = 1/8 - a;
    D(0+1,0+1,0+1,1+1) = 1/8 + b;
    D(0+1,1+1,0+1,1+1) = 1/8 - b;
    D(0+1,0+1,1+1,0+1) = 1/8 + c;
    D(0+1,1+1,1+1,0+1) = 1/8 - c;
    D(1+1,2+1,1+1,1+1) = 1/4;
    

end






