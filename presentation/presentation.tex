% Created 2017-02-20 Mo 09:09
% Intended LaTeX compiler: pdflatex
\documentclass[presentation]{beamer}
\usepackage[utf8]{inputenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\tolerance=1000
\usepackage{color}
\usepackage{listings}
\usepackage{multimedia}
\usepackage[round]{natbib}
\usetheme{Goettingen}
\usecolortheme{rose}
\AtBeginSection[]{
\begin{frame}
\vfill
\centering
\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
\usebeamerfont{title}\secname\par%
\end{beamercolorbox}
\vfill
\end{frame}
}
\makeatletter
\setbeamertemplate{sidebar \beamer@sidebarside}%{sidebar theme}
{
\beamer@tempdim=\beamer@sidebarwidth%
\advance\beamer@tempdim by -6pt%
\insertverticalnavigation{\beamer@sidebarwidth}%
\vfill
\ifx\beamer@sidebarside\beamer@lefttext%
\else%
\usebeamercolor{normal text}%
\llap{\usebeamertemplate***{navigation symbols}\hskip0.1cm}%
\vskip2pt%
\fi%
}%
\addtobeamertemplate{block begin}{
\setlength\abovedisplayskip{-2.5ex plus1ex minus1ex}
\setlength\abovedisplayshortskip{-2.5ex plus1ex minus1ex}
\setlength\belowdisplayskip{0ex plus1ex minus1ex}
\setlength\belowdisplayshortskip{0ex plus1ex minus1ex}
}
\makeatother
\usetheme{default}
\author{Maik Schünemann}
\date{\today}
\title{Optimizing neural goal functions based on Partial Information Decomposition}
\hypersetup{
 pdfauthor={Maik Schünemann},
 pdftitle={Optimizing neural goal functions based on Partial Information Decomposition},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.1.50.2 (Org mode 9.0.5)}, 
 pdflang={Germanb}}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}


\section{Introduction}
\label{sec:org1834227}
\subsection{Partial Information Decomposition}
\label{sec:org0265915}
\begin{frame}[label={sec:org0a64c48}]{Partial Information Decomposition}
\begin{itemize}
\item Classical Information Theory
\begin{itemize}
\item Channel with one input and one output
\item What about two different (groups of input)?
\end{itemize}
\item Partial Information Decomposition \(\text{\tiny{\citep{williams2010nonnegative}}}\)
\begin{itemize}
\item How much information about the output is contained
\begin{itemize}
\item In one input but not the other
\item In both inputs
\item Only available if the inputs are considered jointly
\end{itemize}
\end{itemize}
\end{itemize}
\begin{align*}
     I(Y:(X_1,X_2)) &= I^{\text{unq}}(Y:X_1 \setminus X_2) + I^{\text{unq}}(Y:X_2 \setminus X_1) \\
                & + I^{\text{shd}}(Y:X_1;X_2) + I^{\text{syn}}(Y:X_1;X_2) \\ \\
     I(Y,X_1) &= I^{\text{unq}}(Y:X_1 \setminus X_2) + I^{\text{shd}}(Y:X_1;X_2) \\
     I(Y,X_2) &= I^{\text{unq}}(Y:X_2 \setminus X_1) + I^{\text{shd}}(Y:X_1;X_2) \\
\end{align*}
\end{frame}
\begin{frame}[label={sec:org2c48cdd}]{Bivariate PID Axioms \(\text{\tiny{\citep{bertschinger2014quantifying}}}\)}
\begin{itemize}
\item Symmetry: \(I^{\text{shd}}(Y,X_1,X_2) = I^{\text{shd}}(Y,X_2,X_1)\)
\item Bivariate monotonicity: \(I^{\text{shd}}(Y:X_1,X_2) \leq I(Y:X_1)\), with equality if \(X_1 = F(X_2)\)
\end{itemize}
\end{frame}

\subsection{Neural Goal Functions}
\label{sec:org2ecefa8}
\begin{frame}[label={sec:org8abee84}]{Motivation for neural goal functions}
\begin{itemize}[<+->]
\item six layered cortical mini-columns repeated throughout the neocortex
\item Is the local information processing based on a common underlying
principle that can be described by maximizing a goal function?
\item Neurons receive not only input from their receptive fields but
are also modulated by the current context \(\text{\tiny{\cite{phillips2015functions}}}\)
\begin{itemize}
\item pyramidal cells in layer 4 have two distinct integration sites
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org3a1ec25}]{Coherent Infomax \(\text{\tiny{\citep{kay1997activation}}}\)}
\begin{align*}
F = &\Phi_0 I(Y : X_1 : X_2) + \Phi_1(I:X_1|X_2) + \Phi_2 I(Y:X_2|X_1)  \\
    + &\Phi_3 H(Y|X_1,X_2)
\end{align*}
\begin{itemize}[<+->]
\item Transmit information from driving input that is supported by the context input
\item Transmit some unique information in the driving input
\item Minimize transmission of unique information in the context input
\item Minimize unused output bandwidth.
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orgf48254e}]{Coherent Infomax}
\begin{itemize}
\item Has been investigated by training neural architectures to maximise \(F\)
\item Performs unsupervised learning using contextual guidance
\item Neurons learn to encode different aspects of (correlated) receptive fields
\item Output becomes robust to damage \(\text{\tiny{\citep{kay1998contextually}}}\)
\end{itemize}
\end{frame}
\begin{frame}[label={sec:orgc486e9c}]{Goal functions based on PID \cite{wibral2015partial}}
\begin{align*}
G = &\Gamma_0 I^{\text{unq}}(Y:X_1 \setminus X_2) +\Gamma_1 I^{\text{unq}}(Y:X_2 \setminus X_1) \\
    &+ \Gamma_2 I^{\text{shd}}(Y:X_1;X_2) + \Gamma_3 I^{\text{syn}}(Y:X_1;X_2)  \\
    &+ \Gamma_4 H(Y|X_1,X_2)
\end{align*}
\begin{itemize}[<+->]
\item Coherent Infomax and Predictive Coding can be formulated as
optimizing specific PID Goal functions
\item Coding with Synergy: 
\([\Gamma_0,\Gamma_1,\Gamma_2,\Gamma_3,\Gamma_4] = [1,-1,1,1,-1]\)
\item Motivation: Investigate Effects of Goal functions on simulated
neural architectures
\begin{itemize}
\item Learning rule for neural weights to optimize G
\end{itemize}
\end{itemize}
\end{frame}
\section{Calculating and optimizing PID terms}
\label{sec:orgbb43022}
\subsection{Calculating PID Terms}
\label{sec:org828617c}
\begin{frame}[label={sec:orgff35f65}]{PID Measure \(\text{\tiny{\cite{bertschinger2014quantifying}}}\)}
\[\Delta := \text{set of probability distributions on \(Y\times X_1 \times X_2\)}\]
\[\Delta_p := \{Q \in \Delta : Q_{Y,X_1} = p_{Y,X_1} \wedge Q_{Y,X_2} = p_{Y,X_2}  \} \]

\begin{align*}
I^{\text{unq}}_p(Y:X_1 \setminus X_2) &= \min_{Q \in \Delta_p}I_Q(Y:X_1|X_2) \\
I^{\text{unq}}_p(Y:X_2 \setminus X_1) &= \min_{Q \in \Delta_p}I_Q(Y:X_2|X_1) \\
I^{\text{shd}}_p(Y:X_1;X_2) &= \max_{Q \in \Delta_p}I_Q(Y;X_1;X_2) \\
I^{\text{syn}}_p(Y:X_1;X_2) &= I_p(Y:(X_1,X_2)) - \min_{Q \in \Delta_p}I_Q(Y:(X_1,X_2))
\end{align*}
\begin{itemize}
\item Optimization problems are (not necessarily strictly) convex and equivalent to \(\max_{Q\in \Delta_p}H_Q(Y|X_1,X_2)\)
\end{itemize}
\end{frame}
\begin{frame}[label={sec:org763c574}]{Implementing the convex optimization problem}
\begin{block}{Direct approach}
\begin{itemize}
\item Optimize over \(R^{|X||Y||Z|}\)
\item Lower Bound 0, Upper Bound 1 in each coordinate
\item Linear Equality conditions: 
\begin{itemize}
\item \(|X||Y| + |X||Z|\) conditions for the pairwise marginals
\item 1 Equation for normalization condition
\end{itemize}
\end{itemize}
\end{block}
\begin{block}{Dimensionality reduction}
\begin{itemize}
\item pick particular basis of \(\Delta_p\)
\item Optimize over \(R^{|X|(|Y| - 1)(|Z| - 1)}\)
\item Only linear inequality conditions to satisfy the bounds needed.
\item explicitly specify gradient and Hessian
\end{itemize}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgfacc62a}]{Decomposition of \(\Delta\)}
\begin{itemize}
\item \(\Delta_p = ((p + \ker{A}) \cap \Delta)\),\\
where \(A\) maps \(p_{Y,X_1,X_2}\) to \((p_{Y,X_1},p_{Y,X_2})\)
\item \(\Delta = (\ker{A} \oplus (\Delta / \ker{A})) \cap \Delta\)
\item \(\operatorname{dim}(V) = |X||Y| + |X||Z| - |X| - 1\) with basis 
\begin{align*}
  \{&\left (p_y(i) \right )_{i = 1\ldots |Y| - 1}, \\
    &\left (p_{x_1|y=i}(j)) \right )_{i = 1\ldots |Y|,j = 1\ldots |X_1|-1},\\
    &\left (p_{x_2|y=i}(j)) \right )_{i = 1\ldots |Y|,j = 1\ldots |X_2|-1}  \}
 \end{align*}
\item \(\Gamma = (\gamma_{y;x^0_1;x_1;x^0_2,x_2})_{y,x^0_1 \neq x_1,x^0_2 \neq x_2 }\) basis of \(\ker{A}\)
with \(\gamma_{y;x^0_1;x_1;x^0_2;x_2} = \delta_{y,x^0_1,x^0_2} + \delta_{y,x_1,x_2} - \delta_{y,x_1,x^0_2} - \delta_{y,x^0_1,x_2}\)
\end{itemize}
\begin{align*}
      Q &= Q_Y Q_{X_1|Y} Q_{X_2|Y} + \Gamma \gamma \\
        &= Q_Y Q_{X_1|Y} Q_{X_2|Y} + \sum_{y,x_1 \neq x_1^0,x_2 \neq x_2^0}\gamma_{y,x_1,x_2}\gamma_{y,x_1^0,x_1,x_2^0,x_2}
\end{align*}
\end{frame}

\begin{frame}[label={sec:org6819a6a}]{Gradient and Hessian in \(\Delta_p\)}
\begin{align*} 
\frac{\partial H_q(Y|X_1,X_{2})}{\partial_{\gamma_{y,x_1,x_2}}} = \log{\frac{q(y,x_1,x^0_2)q(y,x^0_1,x_2)q(x^0_{1},x^0_2)q(x_1,x_2)}{q(y,x^0_1,x^0_2)q(y,x_1,x_2)q(x_1,x^0_2)q(x^0_1,x_2)}}
\end{align*}

\begin{align*} 
\frac{\partial^2 H_q(Y|X_1,X_2)}{\partial_{\gamma_{y,x_1,x_2},\gamma_{\tilde{y},\tilde{x_1},\tilde{x_2}}}} &=
 \frac{1}{q(x^0_1,x^0_2)} + \frac{\delta_{x_1,x_2}^{\tilde{x_1},\tilde{x_2}}}{q(x_1,x_2)} + \frac{\delta_{x_2}^{\tilde{x_2}}}{q(x^0_1,x_2)} \\
 & + \frac{\delta_{x_1}^{\tilde{x_1}}}{q(x_1,x^0_2)} - \frac{\delta_{y,x_2}^{\tilde{y},\tilde{x_2}}}{q(y,x^0_1,x_2)} - \frac{\delta_{y,x_1}^{\tilde{y},\tilde{x_1}}}{q(y,x_1,x^0_2)} \\ 
 & - \frac{\delta_{y,x_1,x_2}^{\tilde{y},\tilde{x_1},\tilde{x_2}}}{q(y,x_1,x_2)} - \frac{\delta_{y}^{\tilde{y}}}{q(y,x^0_1,x^0_2)}
\end{align*}
\end{frame}
\subsection{Conditions for non strict convexity}
\label{sec:org5259171}
\begin{frame}[label={sec:org33bda96}]{Conditions for non strict convexity}
\end{frame}
\subsection{Optimizing PID Terms}
\label{sec:orga3c8bdb}
\begin{frame}[label={sec:org1d2ce17}]{Optimizing PID Terms}
\begin{itemize}[<+->]
\item \(\tilde{q}: V \longrightarrow \Delta, \tilde{q}(p) = \operatorname{argmax}_{Q \in \Delta_p}H_Q(Y|X_1,X_{2})\)
\item What is \(\nabla H_{\tilde{q}}(Y|X_1,X_2) = \nabla(H(Y|X_1,X_2) \circ \tilde{q})\)?
\item If \(\tilde{q}\) is defined in a neighborhood of \(p\) in
\(V\) it is differentiable since \(H(Y|X_1,X_2)\) and constraints on \(\Delta_p\) depend smoothly on \(p\)
and thus also \(H_{\tilde{q}}(Y|X_1,X_2)\)
\item Strategy: Approximate \(\nabla H_{\tilde{q}} (Y|X_1,X_2) =
      \nabla(H_{\cdot}(Y|X_1,X_2) \circ \tilde{q})\) by \(\nabla_{V}H_{\tilde{q}(p)}(Y|X_1,X_2)\)
\item Complications:
\begin{itemize}
\item Approximation only valid when optimization problem has unique solution
\item If \(\tilde{q}(p)\) lies on the boundary of \(\Delta_p\), the gradient is not defined
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org36abdbc}]{Local ascent algorithm for \(G\)}
\begin{enumerate}[<+->]
\item Write G in entropic terms
\begin{align*}
 G(p) &= \Gamma_0 I^{\text{unq}}_p(Y:X_1 \setminus X_2) +\Gamma_1 I^{\text{unq}}_p(Y:X_2 \setminus X_1) \\
  &+ \Gamma_2 I^{\text{shd}}_p(Y:X_1;X_2) + \Gamma_3 I^{\text{syn}}_p(Y:X_1;X_2)  \\
  &+ \Gamma_4 H_p(Y|X_1,X_2) \\
  &= (\Gamma_0 - \Gamma_2)H_p(Y|X_2) + (\Gamma_1 - \Gamma_2)H_p(Y|X_1) \\
  &+  \Gamma_2H_p(Y) + (\Gamma_4 - \Gamma_3)H_p(Y|X_1,X_2)\\ 
  &+ (\Gamma_2 + \Gamma_3 - \Gamma_0 - \Gamma_1)H_{\tilde{q}(p)}(Y|X_1,X_2)
\end{align*}
\item Calculate gradients of the \(H_p\) terms
\item solve the optimization problem to obtain \(\tilde{q}(p)\) and move it away from the boundary if necessary
\item Calculate \(\nabla_V H_{\tilde{q}(p)}(Y|X_1,X_2)\)
\item Move \(p\) in the gradient direction
\end{enumerate}
\end{frame}

\begin{frame}[label={sec:org2478b96}]{Efficiency considerations}
\begin{itemize}[<+->]
\item Has to solve Optimization problem in each step
\item Project \(\tilde{q}(p^{t})\) in \(\Delta_{p^(t+1)}\) to get a good starting point
\item Minimum norm or KL projection too costly
\item Let \(\tilde{q}(p^t) = p^t_yp^t_{x_1|y}p^t_{x_2|y} + \Gamma \gamma^t\)\\
\(\gamma^{t+1} = \max_{c\in[0,1]}\left \{\min{p^{t+1}_yp^{t+1}_{x_1|y}p^{t+1}_{x_2|y} + c\Gamma \gamma^t}  \geq \epsilon \right \} \gamma^t\)
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org2ba24e3}]{Optimization example in \(2\times 2 \times 2\)}
\movie[width=\framewidth, height=0.75 \framewidth, showcontrols, poster]{}{video.mp4}
\end{frame}

\section{Learning Network weights}
\label{sec:orgdf845cd}
\subsection{Binary neuron}
\label{sec:org13787f8}
\begin{frame}[label={sec:orgf9fb1a6}]{Neuron with receptive field and contextual input}
\begin{itemize}
\item \(X_1 := \text{driving input from receptive field}\) \(X_2 := \text{contextual input}\)\\
\(Y := \text{ binary output of neuron}\)
\item Two integration sites with different weights
\begin{itemize}
\item \(s_r = \sum_{i=1}^{N_1} w_i(X_1)_i\), \(s_c = \sum_{i = 1}^{N_2}v_i(X_2)_i\)
\end{itemize}
\item Activation function that differentiates driving and contextual input \\
\begin{align*}
P(Y=1|X_1 &= x_1,X_2 = x_2) = \frac{1}{1 + e^{(-A(s_r(x_1),s_c(x_2)))}} =: \theta\\
A(s_r,s_c) &= \frac{1}{2}s_r(1 + e^{2s_rs_c})       
\end{align*}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org614ef87}]{Neuron with receptive field and contextual input}
\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{/home/kima/Dropbox/uni/labrotation_jost/presentation/images/20170217_164413_12253Kcf.png}
\(\text{\tiny{\citep{kay1997activation}}}\)
\end{figure}
\end{frame}

\begin{frame}[label={sec:orga65c486}]{Outlook: Optimizing \(G_{CWS}\)}
\begin{itemize}
\item random distribution \(P_{X_1,X_2}\) and \(w_0,v_0 \sim \operatorname{UNI}[-0.1,0.1]\)
\end{itemize}
\begin{center}
\includegraphics[width=.9\linewidth]{/home/kima/Dropbox/uni/labrotation_jost/G_cws_optim.png}
\end{center}
\end{frame}
\begin{frame}[label={sec:org9306ec3}]{Outlook: Optimizing \(G_{CWS}\)}
\begin{center}
\includegraphics[width=.9\linewidth]{/home/kima/Dropbox/uni/labrotation_jost/G_cws_delta_p.png}
\end{center}
\end{frame}

\section{References}
\label{sec:org0722728}


\nocite{williams2010nonnegative}
\nocite{wibral2015partial}
\nocite{bertschinger2014quantifying}
\nocite{phillips2015functions}
\nocite{kay1997activation}
\nocite{kay1999neural}
\nocite{kay1998contextually}


\begin{frame}[allowframebreaks]
   \frametitle{References}
   \bibliographystyle{plainnat}
   \bibliography{/home/kima/Dropbox/emacs/bibliography/references}
\end{frame}


\section{Appendix}
\label{sec:orgf4fbc69}

\subsection{Calculation of Gradients}
\label{sec:orgc3cee5b}
\begin{frame}[label={sec:org853dc1b}]{\(\nabla_V H(Y|X_1,X_2)\)}
\begin{align*}
\nabla_V H(Y|X_1,X_2) = &- \nabla_Vp_{Y,X_1,X_2} \cdot \log(p_{Y|X_1,X_2} + 1) \\
                        &- \nabla_Vp_{X_1,X_2} \cdot p_{Y|X_1,X_2} 
\end{align*}
\begin{align*}
\partial_{p_Y(i)}p(y,x_1,x_2) &= (\delta_y^i - \delta_y^{|Y|}) p_{X_1|Y=y}(x_1)p_{X_2|Y=y}(x_2) \\
\partial_{p_{X_1|Y=i}(j)}p(y,x_1,x_2) &= \delta_{y}^i(\delta_{x_1}^j - \delta_{x_1}^{|X_1|})p_Y(i)p_{X_2|Y=i}(x_2)\\
\partial_{p_{X_2|Y=i}(j)}p(y,x_1,x_2) &= \delta_{y}^i(\delta_{x_2}^j - \delta_{x_2}^{|X_2|})p_Y(i)p_{X_1|Y=i}(x_1)
\end{align*}
\end{frame}
\begin{frame}[label={sec:orgfb2f323}]{Components of \(\nabla G\) for binary neuron}
\begin{align*}
&\theta := P(Y=1|X_1 = x_1,X_2 = x_2) = \frac{1}{1 + \exp{(-A(s_r(x_1),s_c(x_2)))}}\\
&E_{x_1} := P(Y = 1|X_1 = x_1) = <\theta >_{x_2|x_1}\\
&E_{x_2} := P(Y = 1|X_2 = x_2) = <\theta >_{x_1|x_2}\\
&E       := P(Y = 1) = <\theta>_{x_1,x_2}\\
&\frac{\partial \theta}{\partial w} = \frac{\partial A}{\partial r}\theta (1 - \theta) x_1 \\ 
&\frac{\partial \theta}{\partial v} = \frac{\partial A}{\partial r}\theta (1 - \theta) x_2 \\
\end{align*}
\end{frame}
\begin{frame}[label={sec:org20daa7b}]{Components of \(\nabla G\) for binary neuron}
 \begin{align*} 
 \frac{\partial H(Y|X_1,X_2)}{\partial w} &= \left < \log{\frac{\theta}{1 - \theta}} \frac{\partial A}{\partial s_{x_1}}\theta (1 - \theta) x_1 \right >_{x_1,x_2}\\
 \frac{\partial H(Y|X_2)}{\partial w} &= \left < \log{\frac{E_{x_2}}{1 - E_{x_2}}} \frac{\partial A}{\partial s_{x_1}}\theta (1 - \theta) x_1 \right >_{x_1,x_2}\\
 \frac{\partial H(Y)}{\partial w} &= \left < \log{\frac{E}{1 - E}} \frac{\partial A}{\partial s_{x_1}}\theta (1 - \theta) x_1 \right >_{x_1,x_2}\\
\frac{\partial H_{\tilde{q}(p)}(Y|X_1,X_2)}{\partial w} &= \frac{\partial \theta}{\partial w}  \frac{\partial V}{\partial \theta} \frac{\partial H_{\tilde{q}(p)}(Y|X_1,X_2)}{\partial V}
 \end{align*}
\end{frame}
\end{document}