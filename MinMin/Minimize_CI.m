% to Minimize CI, do a two timestep procedure
p = generate_sample_dist(3,3,3);

[x,fval,exitflag,output,lambda,g,hessian,x_orig] = Min_MI_X_YZ_gamma(p,'interior-point');
fval
for i = 1:10
gb = create_gamma_basis(1,1,3,3,3);
grad = MI_X_YZ_full_gradient(x);
grad_ortho = grad(:) - gb*project_to_gamma(grad,gb);
grad_ortho = (grad_ortho-mean(grad_ortho))./sum(abs(grad_ortho - mean(grad_ortho)));
pnew = p(:) + 0.01*grad_ortho;
pnew = reshape(pnew,[3,3,3]);
% % grad_full = [grad;-sum(grad)];
% % g_full = gb * g;
% % grad_ortho = grad_full - grad_full'*g_full;
% xv = x(:);
% xnew = xv(1:end-1) + 0.0001*grad;%grad_ortho
% xnew = [xnew;1 - sum(xnew)];
% %xnew = xnew(:) + 0.001*grad_ortho;
% xnew = reshape(xnew,[3,3,3]);
% xnew = xnew + min(xnew(:))
% xnew = xnew ./sum(xnew(:));
%xnew = x(:) + 0.01*grad_ortho;
%xnew = reshape(xnew,[3,3,3]);

[x,fval2,exitflag2,output2,lambda2,g2,hessian2,x_orig] = Min_MI_X_YZ_gamma(pnew,'interior-point',1,1,x_orig);
fval2
fval = [fval,fval2];
end

plot(fval);