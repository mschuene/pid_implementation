function [fval,grad] = MI_X_YZ_and_gradient(p)
    fval = MI_X_YZ(p);
    if nargout > 1
        grad = MI_X_YZ_full_gradient(p) -0.442693536686929;
    end
end