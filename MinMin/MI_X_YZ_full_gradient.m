function [grad] = MI_X_YZ_full_gradient(Q,cardx,cardy,cardz)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 2
        [cardx,cardy,cardz] = size(Q);
    end
    grad = zeros(size(Q));
    Q_X = squeeze(sum(sum(Q,3),2));
    Q_YZ = squeeze(sum(Q,1));
    for x = 1:cardx
        for y = 1:cardy
            for z = 1:cardz
                grad(x,y,z) = -1 + ...
                    log2(Q(x,y,z)) - log2(Q_X(x)) - log2(Q_YZ(y,z));
            end
        end
    end
end

