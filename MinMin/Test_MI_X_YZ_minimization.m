s = [8,8,8];
p = generate_sample_dist(8,8,8);
f = @(x) MI_X_YZ_and_gradient(reshape(x,s));
condleq_lhs = ones(1,prod(s));
condleq_rhs = 1;
lb = zeros(prod(s),1);
ub = ones(prod(s),1);

options = optimoptions('fmincon');
options.SpecifyObjectiveGradient = true;
%options.HessianFcn = @(x,~) hess_gamma(x,p,gb,igb,y0,z0);
options.Display = 'iter-detailed';
%options.SubproblemAlgorithm = 'cg';
options.CheckGradients = true;
options.FiniteDifferenceType = 'central';
options.FiniteDifferenceStepSize = 1e-10;
%options.InitBarrierParam = 1;
%options.Algorithm = ;%'sqp';%'active-set';
options.HonorBounds = true;
%options.Diagnostics = 'on';
options.PlotFcn = @optimplotfval;
options.OptimalityTolerance = 1e-6;
options.ConstraintTolerance = 1e-8;
%options.StepTolerance = 1e-12;
%options.UseParallel = true;
%options.MaxFunctionEvaluations = 1000000;
options.MaxIterations = 0;

[x_orig,fval,exitflag,output,lambda,grad,hessian] = ...
        fmincon(f,p,condleq_lhs,condleq_rhs,[],[],lb,ub,[],options);