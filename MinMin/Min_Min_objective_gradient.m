function [fval,grad_ortho] = Min_Min_objective_gradient(p,gb)
    [x,fval,exitflag,output,lambda,g,hessian,x_orig] = Min_MI_X_YZ_gamma(p);
    fval = -fval;
    if nargout > 1
        grad = MI_X_YZ_full_gradient(x);
        grad_ortho = grad(:);%- gb*project_to_gamma(grad,gb);
        %grad_ortho = (grad_ortho-mean(grad_ortho))./sum(abs(grad_ortho - mean(grad_ortho)));
        grad_ortho = -grad_ortho;
    end
end