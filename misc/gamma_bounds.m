function [ lb,ub ] = gamma_bounds(p,igb,y0,z0)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

  if nargin < 3
      y0 = 1; z0 = 1;
  end
  lb = zeros(size(igb,1),1);
  ub = zeros(size(igb,1),1);
  for i = 1:size(igb,1)
     c = igb(i,:);
     ub(i) = min([p(c(1),y0,c(3)),p(c(1),c(2),z0),1-p(c(1),y0,z0),1-p(c(1),c(2),c(3))]);
     lb(i) = -min([1 - p(c(1),y0,c(3)),1 - p(c(1),c(2),z0),p(c(1),y0,z0),p(c(1),c(2),c(3))]);
  end

end

