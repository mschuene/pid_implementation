%test cases
s = [2,2,2];
pand(1 + 1,1 + 1,1 + 1) = 0.25;  
pand(0 + 1,0 + 1,0 + 1) = 0.25;
pand(0 + 1,1 + 1,0 + 1) = 0.25;
pand(0 + 1,0 + 1,1 + 1) = 0.25;

q14 = zeros(s);
q14(1 + 1,1 + 1,1 + 1) = 0.25;  
q14(0 + 1,1 + 1,1 + 1) = 0.25;
q14(0 + 1,0 + 1,0 + 1) = 0.5;

% X = Y xor Z
pxor = zeros(s);
pxor(0 + 1,0 + 1,0 + 1) = 0.25;
pxor(1 + 1,0 + 1,1 + 1) = 0.25;
pxor(0 + 1,1 + 1,1 + 1) = 0.25;
pxor(1 + 1,1 + 1,0 + 1) = 0.25;


[UIY,UIX,SI,CI,Q] = PID(pand);

assert(abs(UIY - 0) < 1e-3)
assert(abs(UIX - 0) < 1e-3)
assert(abs(CI - 0.5) < 1e-3)

[UIY,UIX,SI,CI,Q] = PID(pxor);
assert(abs(CI - 1) < 1e-6)

load Ptest
[UIY,UIX,SI,CI,Q] = PID(Ptest)