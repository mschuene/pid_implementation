n = 3
A = ones(3)
l = 0;
u = 3;
b = zeros(3,1)
cvx_begin
    variable x(n,n)
    y = sum(x,1);
    z = sum(x,2);
    minimize(-x(:)'*sum_log(x(:)) 
    subject to
        l <= x <= u
        sum(x(:)) == 1
cvx_end