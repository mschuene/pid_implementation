function [ cond_lhs,cond_rhs ] = setup_conditions_markov(p)
%UNTITLED2 sets up the conditions for 1
%   Detailed explanation goes here
    cardq = 10; [cardx1,cardx2,cardy] = size(p);
    s = cardq * numel(p);
    cond_lhs = zeros(cardx1*cardx2*cardy + 1,s);
    cond_rhs = zeros(cardx1*cardx2*cardy + 1,1);
    i = 0;
    for x1 = 1:cardx1
        for x2 = 1:cardx2
            for y = 1:cardy
                i = i + 1;
                cond = zeros([cardq,size(p)]);
                cond(:,x1,x2,y) = 1;
                cond_lhs(i,:) = cond(:)';
                cond_rhs(i) = p(x1,x2,y);
            end
        end
    end
    cond_lhs(i + 1,:) = ones(1,s);
    cond_rhs(i + 1) = 1;

end

