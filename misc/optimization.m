
cardx = 2;
cardy = 2;
cardz = 2;

% size of the probability matrix
% Probability distribution specifying the pairwise marginals xy and xz
% denoted as d dimensional vector
s = [cardx,cardy,cardz];
pand = zeros(s);
% binary and (indexing logic_value + 1 because 1 based indexing)
pand(1 + 1,1 + 1,1 + 1) = 0.25;  
pand(1 + 1,0 + 1,0 + 1) = 0.25;
pand(0 + 1,1 + 1,0 + 1) = 0.25;
pand(0 + 1,1 + 1,1 + 1) = 0.25;

por(1 + 1,1 + 1,1 + 1) = 0.25;  
por(1 + 1,0 + 1,1 + 1) = 0.25;
por(0 + 1,1 + 1,1 + 1) = 0.25;
por(0 + 1,0 + 1,0 + 1) = 0.25;




q14 = zeros(s);
q14(1 + 1,1 + 1,1 + 1) = 0.25;  
q14(0 + 1,1 + 1,1 + 1) = 0.25;
q14(0 + 1,0 + 1,0 + 1) = 0.5;

% X = Y xor Z
pxor = zeros(s);
pxor(0 + 1,0 + 1,0 + 1) = 0.15;
pxor(1 + 1,0 + 1,1 + 1) = 0.15;
pxor(0 + 1,1 + 1,1 + 1) = 0.35;
pxor(1 + 1,1 + 1,0 + 1) = 0.35;




%% direct minimization 
[Q_direct,fval,exitflag,output,lambda,grad,hessian,x_orig] = Min_MI_X_YZ_direct(pand);
Q_direct
%% minimization using appropriate basis
[Q_gamma,fval,exitflag,output,lambda,grad,hessian,x_orig] = Min_MI_X_YZ_gamma(pand);
Q_gamma

%% comparision
abs(Q_direct - Q_gamma);

%% Partial information decomposition
% [UIY_and,UIZ_and,SI_and,CI_and] = PID(pand);
% disp('PID of and');
% [UIY_and,UIZ_and,SI_and,CI_and]
[UIY_xor,UIZ_xor,SI_xor,CI_xor] = PID(pxor);
disp('PID of xor');
[UIY_xor,UIZ_xor,SI_xor,CI_xor]


%% what is the argmin point for a uniform distribution
uniform_dist = ones(5,5,5);
uniform_dist = uniform_dist./numel(uniform_dist);
