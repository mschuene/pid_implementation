% solve optimization problem for unique information in naive way

cardx = 2;
cardy = 2;
cardz = 2;

% size of the probability matrix
% Probability distribution specifying the pairwise marginals xy and xz
% denoted as d dimensional vector
s = [cardx,cardy,cardz];
p = zeros(s);
% binary and (indexing logic_value + 1 because 1 based indexing)
p(1 + 1,1 + 1,1 + 1) = 0.25;  
p(0 + 1,0 + 1,0 + 1) = 0.25;
p(0 + 1,1 + 1,0 + 1) = 0.25;
p(0 + 1,0 + 1,1 + 1) = 0.25;

q14 = zeros(s);
q14(1 + 1,1 + 1,1 + 1) = 0.25;  
q14(0 + 1,1 + 1,1 + 1) = 0.25;
q14(0 + 1,0 + 1,0 + 1) = 0.5;


% specify conditions for delta_p
% conditions forp q(x,y) = p(x,y)
% for each x and y combination the sum over all z values has to equal
% p(x,y)

