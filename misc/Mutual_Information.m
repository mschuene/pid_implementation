function [ I ] = Mutual_Information(p)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

    p_x = repmat(sum(p,2),[1,size(p,2)]);
    p_y = repmat(sum(p,1),[size(p,1),1]);
    I = p.*(log2(p) - log2(p_x) - log2(p_y));
    I(isnan(I)) = 0;
    I = real(sum(I(:)));

end

