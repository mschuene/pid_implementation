
% now do optimization in suitable basis with explicit gradient

y0 = 0 + 1; z0 = 0 + 1;
gb = create_gamma_basis(y0,z0);
% remaining conditions in the new basis are just the bounds which now are
% linear inequalities
[ condleq_lhs,condleq_rhs ] = setup_delta_p_conditions_gamma(p,gb)

% all probablity distributions are now expressible as p + gamma_basis * x  
initial_value = zeros(size(gb,2),1);
f = @(x) gamma_objective_gradient(x,p,gb,y0,z0,cardx,cardy,cardz)
options = optimoptions('fmincon');
options.SpecifyObjectiveGradient = true;
options.Algorithm = 'sqp'%'active-set';
% options.MaxFunctionEvaluations = 1000000;
% options.MaxIterations = 10000;

[x,fval,exitflag,output,lambda,grad,hessian] = fmincon(f,initial_value,condleq_lhs,condleq_rhs,[],[],[],[],[],options);
x,fval


