function [ MI ] = MI_Q_Y(p)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
     cardQ = 10;cardx1 = 2; cardx2 = 2; cardy = 2;
     p = reshape(p,[cardx1,cardx2,cardy,cardQ]);
     p_Y_Q = squeeze(sum(sum(p,1),2));
     p_Q = repmat(sum(p_Y_Q,1),[cardy,1]);
     p_Y = repmat(sum(p_Y_Q,2),[1,cardQ]);
     MI = p_Y_Q.*(log2(p_Y_Q) - log2(p_Y) - log2(p_Q));
     MI(isnan(MI)) = 0;
     MI = real(sum(MI(:)));
end

