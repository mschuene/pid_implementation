
p = zeros(2,2,2);
p(0+1,0+1,0+1) = 0.499;
p(0+1,1+1,0+1) = 0.001;
p(1+1,1+1,1+1) = 0.5;


[cond_lhs,cond_rhs] = setup_conditions_markov(p);
f = @(Q) -MI_Q_Y(reshape(Q,10,2,2,2));

Q_start = zeros(10,2,2,2);

% Q_start(:,0+1,0+1,0+1)=0.499/10;
% Q_start(:,0+1,1+1,0+1)=0.001/10;
% Q_start(:,1+1,1+1,1+1)=0.5/10;



options = optimoptions('fmincon');
%options.SpecifyObjectiveGradient = true;
%options.HessianFcn = @(x,~) hess_gamma(x,p,gb,igb,y0,z0);
options.Display = 'iter-detailed';
%options.OutputFcn = @outfun;
%options.SubproblemAlgorithm = 'cg';
%options.CheckGradients = true;
%options.InitBarrierParam = 1;
options.Algorithm = 'sqp';
%options.HonorBounds = true;
options.PlotFcn = @optimplotfval;
options.OptimalityTolerance = 1e-6;
options.ConstraintTolerance = 1e-6;
%options.StepTolerance = 1e-12;
%options.UseParallel = true;
%options.MaxFunctionEvaluations = 1000000;
%options.MaxIterations = 10000;

 [x_orig,fval,exitflag,output,lambda,grad,hessian] = ...
        fmincon(f,Q_start(:),[],[],cond_lhs,cond_rhs,...
                zeros(size(Q_start(:))),ones(size(Q_start(:))),@nonlcon,options);
  fval
