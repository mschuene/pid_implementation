Implementation of Partial Information Decomposition.

Main function is in PID.m

see test_cases.m for example calls.

Currently, adjust optimization options manually in Min_MI_X_YZ_gamma.m and in the
gradient and hessian computations. Current Values seem to work fine for most cases
(randomly generated distributions and simple example problems).

The sqp algorithms handles small examples very well even if they don't have full support while
the interior point algorithm can handle larger distributions.

On top of that I am developing appropriate gradient functions for the pid decomposition so that goal functions that depend on PID terms can be optimized.


{(a*(1-b)*(1-d)+f)/(a*b*d+f+(1-a)*c*e+g)==(a*(1-b)*d-f)/(a*(1-b)*d-f+(1-a)*(1-c)*e-g),(a*(1-b)*(1-d)+f)/(a*(1-
b)*(1-d)+f+(1-a)*(1-c)*(1-e)+g)==(a*(1-b)*d-f)/(a*(1-b)*d-f+(1-a)*(1-c)*e-g)}


