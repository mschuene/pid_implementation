%% params for and
%a = 3./4; b = 2./3; d = 2./3;c = 1e-2; e = 1e-1;
%a = 3./4; b = 2./3; d = 2./3; c = 0.4; e = 0.4;
%a = 1./4; b = 0.85; d = 0.9; c = 0.33; e = 0.35;
%c=0;e=0;
a = rand
b = rand
c = rand
d = rand
e = rand

%a = 1/2 + 1e-3;b=1/2 + 1e-3;c=1/2;d=1/2;e=1/2;
%a = 1/2;b=rand;c=b;d=rand;e=d;

%a = 1/2 + 1e-3;b=1/2 + 1e-3;c=1/2 ;d=1/2 ;e=1/2 -1e-3 ;


flb = -min([a.*b.*d,a.*(1-b).*(1-d)]);fub = min([a.*b.*(1-d),a.*(1-b).*d]);
glb = -min([(1-a).*c.*e,(1-a).*(1-c).*(1-e)]);gub=min([(1-a).*c.*(1-e),(1-a).*(1-c).*e]);

fl = linspace(flb,fub,100);
gl = linspace(glb,gub,100);
[f,g] = ndgrid(fl,gl);

px = a;
pygx = [b;c];
pzgx = [d;e];
GB = create_gamma_basis(1,1,2,2,2);
x_orig = zeros(size(GB,2),1);
%%
for i = 1:1000
pand_start = starting_dist(px,pygx,pzgx);
[e,x_orig] = proj_deltap_inner(reshape(pand_start(:) + GB*x_orig,size(pand_start)),GB,x_orig);
e,x_orig
[UIY,UIZ,SI,CI,Q,fval,exitflag,output,lambda,grad,hessian,x_orig,HISTORY] = PID(pand_start,'interior-point');
path = HISTORY.x';

a = px; b = pygx(1); c = pygx(2);d = pzgx(1);e=pzgx(2);
flb = -min([a.*b.*d,a.*(1-b).*(1-d)]);fub = min([a.*b.*(1-d),a.*(1-b).*d]);
glb = -min([(1-a).*c.*e,(1-a).*(1-c).*(1-e)]);gub=min([(1-a).*c.*(1-e),(1-a).*(1-c).*e]);

fl = linspace(flb,fub,100);
gl = linspace(glb,gub,100);

MI = bsxfun(@(a,b) arrayfun(@(c) MI_X_YZ(pand_start(:) + GB*[a;c]),b),fl,gl');
figure(4)
surf(fl,gl,MI);
xlabel('\lambda_1');
ylabel('\lambda_2');
colorbar;
hold on;
plot3(path(:,1),path(:,2),ones(size(HISTORY.x,2)),'r')
hold off;
title(strcat('min MI(X;Y,Z) =  ',num2str(MI_X_YZ(pand_start(:) + GB*x_orig)),' abcde= ',num2str([a,b,c,d,e])));
view(2)
drawnow;
[Q_inner,~] = proj_deltap_inner(Q,GB,x_orig);
[dhdx,dhdygx,dhdzgx] = dhddeltapo(Q_inner);
px = min(max(px + 0.01* dhdx,0),1)
pygx = min(max(pygx + 0.01 * dhdygx,0),1)
pzgx = min(max(pzgx + 0.01 * dhdzgx,0),1)


end