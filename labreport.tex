% Created 2017-04-17 Mo 22:43
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\tolerance=1000
\usepackage{color}
\usepackage{listings}
\usepackage{natbib}
\usepackage{float}
\usepackage{parskip}
\usepackage{bbm}
\usepackage{dsfont}
\usepackage{amsmath,amscd}
\usepackage{breqn}
\usepackage{amsthm}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem*{cor}{Corollary}
\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{conj}{Conjecture}[section]
\newtheorem{exmp}{Example}[section]
\theoremstyle{remark}
\newtheorem*{rem}{Remark}
\newtheorem*{note}{Note}
\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}
\author{Maik Schünemann}
\date{\today}
\title{Optimizing neural goal functions based on Partial Information Decomposition}
\hypersetup{
 pdfauthor={Maik Schünemann},
 pdftitle={Optimizing neural goal functions based on Partial Information Decomposition},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.1.50.2 (Org mode 9.0.5)}, 
 pdflang={Germanb}}
\begin{document}

\maketitle
\tableofcontents

\section{Abstract}
\label{sec:org72ea275}
Neural systems process information merging bottom-up (sensory) input
and top-down or lateral contextual input from other brain regions or
neighboring neurons. Partial Information Decomposition
\cite{williams2010nonnegative} allows an information theoretic
analysis of a channel receiving two distinct input (groups)
producing one output and provides a unifying framework to specify
neural goal functions. This work uses the measure proposed by
\cite{bertschinger2014quantifying}, which is based on a convex
optimization problem, to derive an approximative gradient ascend
learning rule for a goal function given by a linear combination of
Partial Information Decomposition terms. It is shown that the convex
optimization problem has in general no unique solution. In addition,
properties of the convex optimization problem are studied. For the
special case of binary distributions it can be solved exactly and
general conditions in which the problem has no unique solution are
derived.

\section{Introduction}
\label{sec:org3b88b25}

The brain processes information in a highly recurrent manner. Of
remarkable interest is the occurrence of repeated anatomical
structures, like the cortical columns throughout the neo-cortex.
This suggest that these structures may operate in an unsupervised
manner according to some local objective and that their information
processing can be described by optimization of a specific goal
function. \cite{wibral2015partial} propose Partial Information
Decomposition as a unified approach to specify such goal functions
and show that existing theories about information processing in the
brain, for instance Coherent Infomax (\cite{kay1999neural}) can be
expressed with goal functions in the Partial Information
Decomposition framework.

Whereas Coherent Infomax is based on classical information theoretic
measures and provides gradient ascend learning rules for neural
architectures, classical information theory is not enough to specify
a measure for Partial Information Decomposition and currently no
single measure is generally accepted.

For a channel receiving two separate (possibly multivariate) inputs
\(X_1,X_2\) and providing one output \(Y\), modelled by discrete
random variables, Partial Information Decomposition splits the
Mutual Information into the unique information
\(I^{\text{unq}}\) in one channel that is not in the other, shared
information \(I^{\text{shd} }\) that is redundantly encoded in
both \(X_1\) and \(X_2\) and synergistic information
\(I^{\text{syn}}\) that is only available when considering the
inputs jointly.
These terms have to be non-negative and must satisfy

\begin{align}
\label{eq:decomposition}
  I(Y:(X_1,X_2)) &= I^{\text{unq}}(Y:X_1 \setminus X_2) + I^{\text{unq}}(Y:X_2 \setminus X_1) \nonumber \\
             & + I^{\text{shd}}(Y:X_1;X_2) + I^{\text{syn}}(Y:X_1;X_2) \\ \nonumber \\
  I(Y,X_1) &= I^{\text{unq}}(Y:X_1 \setminus X_2) + I^{\text{shd}}(Y:X_1;X_2) \\
  I(Y,X_2) &= I^{\text{unq}}(Y:X_2 \setminus X_1) + I^{\text{shd}}(Y:X_1;X_2) \text{ .}
  \end{align}

A \emph{goal function} based on the Partial Information Decomposition
terms is written as

 \begin{align}
 \label{eq:goal-function}
G = &\Gamma_0 I^{\text{unq}}(Y:X_1 \setminus X_2) +\Gamma_1 I^{\text{unq}}(Y:X_2 \setminus X_1) \nonumber  + \\
    &\Gamma_2 I^{\text{shd}}(Y:X_1;X_2) + \Gamma_3 I^{\text{syn}}(Y:X_1;X_2)  +\nonumber\\
    &\Gamma_4 H(Y|X_1,X_2) \text{, }
\end{align}
with scalar parameters \(\Gamma_1,\ldots,\Gamma_4\).


This work uses the Partial Information Decomposition measure
proposed by \cite{bertschinger2014quantifying}, which is defined by equations \eqref{eq:def}-\eqref{eq:def-end}. \\


\[\Delta := \text{set of probability distributions on \(Y\times X_1 \times X_2\)}\]
\[\Delta_p := \{Q \in \Delta : Q_{Y,X_1} = p_{Y,X_1} \wedge Q_{Y,X_2} = p_{Y,X_2}  \} \]
\begin{align}
\label{eq:def}
I^{\text{unq}}_p(Y:X_1 \setminus X_2) &= \min_{Q \in \Delta_p}I_Q(Y:X_1|X_2) \\
I^{\text{unq}}_p(Y:X_2 \setminus X_1) &= \min_{Q \in \Delta_p}I_Q(Y:X_2|X_1) \\
I^{\text{shd}}_p(Y:X_1;X_2) &= \max_{Q \in \Delta_p}(I_Q(Y,X_1) - I_Q(Y,X_2|X_1)) \\
I^{\text{syn}}_p(Y:X_1;X_2) &= I_p(Y:(X_1,X_2)) - \min_{Q \in \Delta_p}I_Q(Y:(X_1,X_2))
\label{eq:def-end}
\end{align}

The Mutual Information, Entropy and Partial Information
Decomposition measures are functions of the distribution of their
arguments. As the Partial Information Decomposition terms depend
on information theoretic measures evaluated at different
distributions, they are made explicit as subscripts in the
notation.


These optimization problems are equivalent to maximizing
\(H(Y|X_1,X_2)\) over \(\Delta_p\) which can be seen by writing
the mutual information terms as combinations of conditional
entropies.

Different to Coherent Infomax, gradients of the Partial
Information Decomposition terms are not easily obtained, because
changes in \(p\) lead to different convex spaces \(\Delta_p\)
for the optimization problem.

The rest of this work is organized as follows. A numerical
implementation of the convex optimization problem is given in
\ref{sec:calc}. In Section \ref{sec:analytical}, two analytical
properties of this optimization problem, general conditions for
non-unique solutions and full treatment of the 2x2x2 binary case
are investigated. The strategy to approximate the gradients of the
Partial Information Decomposition terms is detailed in section
\ref{sec:approx} and applied for a simple neural architecture in
section \ref{sec:learning}. This section also compares the resulting
learning rules with the learning rules of coherent infomax and
describes the conditions for goal functions that can not be
formulated in the Coherent Infomax framework. As proof of concept
for application of the  learning rule, the recently proposed goal
function Coding with Synergy (\cite{wibral2015partial}) is optimized
for a simple neural architecture.


\section{Calculating Partial Information Decomposition \label{sec:calc}}
\label{sec:org021d941}

To calculate the Partial Information Decomposition, the equations
\eqref{eq:def}-\eqref{eq:def-end} can be written in form of classical
information theoretic quantities that are invariant in \(\Delta_p\)
and the result of \(\max_{Q\in \Delta_p}{H_Q(Y|X_1,X_2)}\)

\begin{align} \label{eq:pid-info}
  I^{\text{unq}}_p(Y:X_1 \setminus X_2) &= H_p(Y|X_2) - \max_{Q \in \Delta_p}H_{Q}(Y|X_1,X_2) \\
  I^{\text{unq}}_p(Y:X_2 \setminus X_1) &= H_p(Y|X_1) - \max_{Q \in \Delta_p}H_Q(Y|X_1,X_2) \\
  I^{\text{shd}}_p(Y:X_1;X_2) &= H_p(Y) - H_p(Y|X_1) - H_p(Y,X_2) + \max_{Q \in \Delta_p}H_Q(Y|X_1,X_2)    \\
  I^{\text{syn}}_p(Y:X_1;X_2) &= H_p(Y|X_1,X_2)  - \max_{Q \in \Delta_p}H_Q(Y|X_1,X_2) \text{ .}
  \label{eq:pid-info-end}
\end{align}

The optimization domain \(\Delta_p\) can be described as follows
\cite[][A.1]{bertschinger2014quantifying}. Let \(A:\Delta =
  \Delta_{Y,X_1,X_2} \rightarrow (\Delta_{Y,X_1},\Delta_{Y,X_2}), \hspace{3mm}
  A(Q_{Y,X_1,X_2}) = (Q_{Y,X_1},Q_{Y,X_2})\) be the linear map that
turns a distribution on \((Y,X_1,X_2)\) to the pairwise marginal
distributions on \((Y,X_1)\) and \((Y,X_2)\). \(\Delta_p\) can
thus be seen as an intersection between an affine space and a
simplex \(\Delta_p = (p + \ker{A}) \cap \Delta\), which makes it a
convex polytope. Together with the concavity of \(H(Y|X_1,X_2)\) on
\(\Delta\) this proves that the optimization problems are convex.


The optimization problem can be solved numerically with a
constrained minimization procedure like matlab's
\(\operatorname{fmincon}\). By specifying a basis for \(\ker A\)
and optimizing in this basis, the dimensionality of the optimization
problem can be reduced. 

A basis for \(\ker{A}\) is given by \citep{bertschinger2014quantifying}

\[\Gamma = (\gamma_{y;x^0_1;x_1;x^0_2,x_2})_{y,x^0_1 \neq x_1,x^0_2 \neq x_2 } \text{, }\]

where \(\gamma_{y;x^0_1;x_1;x^0_2,x_2}\) is a probability distribution with four nonzero entries

\[\gamma_{y;x^0_1;x_1;x^0_2;x_2} = \delta_{y,x^0_1,x^0_2} + \delta_{y,x_1,x_2} - \delta_{y,x_1,x^0_2} - \delta_{y,x^0_1,x_2} \text{ .}\]

The dimensionality of \(\ker{A}\) is thus \(|Y|(|X_1|-1)(|X_2| -1)\). The remaining \(|X||Y| + |X||Z| - |X| - 1\) dimensions specify
the starting distribution \(p\) of each \(\Delta_p = (p + \ker{A})
  \cap \Delta\). Two distributions \(p,\tilde{p}\) with \(p_Y =
  \tilde{p}_Y,p_{Y|X_{1}} = \tilde{p}_{Y|X_{1}},p_{Y|X_2} =
  \tilde{p}_{Y|X_2}\) give rise to the same \(\Delta_p =
  \Delta_{\tilde{p}}\). Using this equivalence relation, \(\Delta_p\) can be specified by a point in the quotient space \(V := \Delta /
  \ker{A}\), which can be parameterized by

\begin{align*}
      \{&\left (p_y(i) \right )_{i = 1\ldots |Y| - 1}, \\
        &\left (p_{x_1|y=i}(j)) \right )_{i = 1\ldots |Y|,j = 1\ldots |X_1|-1},\\
        &\left (p_{x_2|y=i}(j)) \right )_{i = 1\ldots |Y|,j = 1\ldots |X_2|-1}  \}\text{ .}
 \end{align*}

A general distribution \(Q \in \Delta\) can be written as 
\begin{align}
\label{eq:decomposition}
 Q &= Q_0 + \sum_{y,x_1',x_2'}\gamma_{y,x_1',x_2'}\gamma_{y;x^0_1;x_1;x^0_2,x_2} \\
 Q &= Q_Y Q_{X_1|Y} Q_{X_2|Y} + \Gamma \gamma
\end{align}
with \(Q_0 \in V\) and \(\gamma =
  (\gamma_{y,x_1',x_2'})_{y,x_1'\neq x_1^0,x_2' \neq x_2^0}\)
denoting the coefficients with respect to the \(\Gamma\) basis of \(\Delta_Q\).

This decomposition \(\Delta = (\ker{A} \oplus V)
  \cap \Delta\) will be used throughout the rest of this paper.

To speed up the optimization procedure, the gradient \eqref{eq:grad} and
hessian \eqref{eq:hess} of \(H(Y|X_1,X_2)\) can be used, where \(c\)
accounts for the basis of the logarithm (\(c=1\) for natural
logarithm and \(c= \frac{1}{\ln{2}}\) for binary logarithm).

\begin{align} 
\label{eq:grad}
\frac{\partial H_q(Y|X_1,X_{2})}{\partial_{\gamma_{y,x_1,x_2}}} = \log{\frac{q(y,x_1,x^0_2)q(y,x^0_1,x_2)q(x^0_{1},x^0_2)q(x_1,x_2)}{q(y,x^0_1,x^0_2)q(y,x_1,x_2)q(x_1,x^0_2)q(x^0_1,x_2)}}
\end{align}


\begin{align} 
 \label{eq:hess}
 \frac{\partial^2 H_q(Y|X_1,X_2)}{\partial_{\gamma_{y,x_1,x_2},\gamma_{\tilde{y},\tilde{x_1},\tilde{x_2}}}} = c & \Bigg (
  \frac{1}{q(x^0_1,x^0_2)} + \frac{\delta_{x_1,x_2}^{\tilde{x_1},\tilde{x_2}}}{q(x_1,x_2)} + \frac{\delta_{x_2}^{\tilde{x_2}}}{q(x^0_1,x_2)}
  + \frac{\delta_{x_1}^{\tilde{x_1}}}{q(x_1,x^0_2)} - \frac{\delta_{y,x_2}^{\tilde{y},\tilde{x_2}}}{q(y,x^0_1,x_2)}\\ & - \frac{\delta_{y,x_1}^{\tilde{y},\tilde{x_1}}}{q(y,x_1,x^0_2)} 
   - \frac{\delta_{y,x_1,x_2}^{\tilde{y},\tilde{x_1},\tilde{x_2}}}{q(y,x_1,x_2)} - \frac{\delta_{y}^{\tilde{y}}}{q(y,x^0_1,x^0_2)} \Bigg )
 \end{align}                                                                                                                                      


The constraints for the optimization problem with basis \(\Gamma\)
reduce to checking whether for the current point \(\gamma\) all
components of \(p + \Gamma \gamma\) are greater than\(0\). For
the numerically implementation, this leads to constraints given by
the block matrix form

\begin{align}
\Delta_p = \bigg \{\gamma \in \operatorname{span}{G}: \begin{pmatrix} G \\ -G
\end{pmatrix} \gamma \leq \begin{pmatrix} 1 - p \\ p \end{pmatrix} \bigg \} \text{ .}
\end{align}

\section{Properties of the optimization problem \label{sec:analytical}}
\label{sec:org0e51304}
Before describing the approximation rule for the gradient of the
Partial Information Decomposition this section discusses analytical
properties of the optimization problem \(\max_{Q \in
  \Delta_p}{H(Y|X_1,X_2)}\).

\subsection{Uniqueness}
\label{sec:org425ddbd}

Numerically, for most starting points \(p\), the solution
\(\tilde{q} = \operatorname{argmax}_{Q \in
   \Delta_p}{H(Y|X_1,X_2)}\). is locally unique. As the set of maxima
of the convex function \(H(Y|X_1,X_2)\) is itself convex, this
observed local uniqueness implies global uniqueness. However, there
are families of \(p\) in \(V\) for which a whole
subspace in \(\Delta_p\) attaines \(\max_{Q\in \Delta_p}H_Q(Y|X_1,X_2)\). \\


\begin{thm}
Let \(p\) have full support and \(p_Y \independent p_{X_1}\) and
\(p_Y \independent p_{X_2}\). Then \(\operatorname{argmax}_{Q\in
   \Delta_p}{H_Q(Y|X_1,X_2)}\) is not unique.
\end{thm}

\begin{proof}
Construct \(Q_0 \in \Delta_p\) by \(Q_0 = p_Yp_{X_1|Y}p_{X_2|Y} =
   p_Yp_{X_1}p_{X_2}\). By construction \(Q_0 \independent
   (Q_{0_{X_1}},Q_{0_{X_2}})\), which implies that \(Q_0\) is a
maximizer of \(H(Y|X_1,X_2)\) as \(H_{Q_0}(Y|X_1,X_2) = H(Y)\).

Using the Decomposition \ref{eq:decomposition}, one can derive conditions for which 
\(Q \independent (Q_{X_1},Q_{X_2})\).

   \begin{align*}
   Q(y,x_1,x_2) = 
\begin{cases}
        p(y)p(x_1)p(x_2) + \gamma_{y,x_1,x_2}  &\mbox{if } x_1 \neq x^0_1 \text{ and } x_2 \neq x^0_2 \\
        p(y)p(x_1)p(x_2) + \sum_{x_1',x_2'} \gamma_{y,x_1',x_2'} &\mbox{if } x_1 = x^0_1 \text{ and } x_2 = x^0_2 \\
        p(y)p(x_1)p(x_2) + \sum_{x_1'} \gamma_{y,x_1',x_2} &\mbox{if } x_1 = x^0_1 \text{ and } x_2 \neq x^0_2 \\
        p(y)p(x_1)p(x_2) + \sum_{x_2'} \gamma_{y,x_1,x_2'} &\mbox{if } x_1 \neq x^0_1 \text{ and } x_2 = x^0_2 \\
\end{cases}
   \end{align*}
\(Q\) is independent of  \((Q_{X_1},Q_{X_2})\) if and only if for all \(x_1,x_2\)
\(Q(x_1,x_2|y)\) is constant with respect to \(y\).
This condition is fulfilled if 
\[\frac{\gamma_{y,x_1,x_2}}{p_y} = \operatorname{const} \text{ } \forall  y \text{ ,}\]
which can be attained in a neighborhood of \(Q_0\) because
\(p\) has full support and \(Q_{0}\) lies in the interior of \(\Delta_p\).
\end{proof}


\subsection{Analysis of the 2x2x2 binary case}
\label{sec:org91e40d3}

In the case of \(2\times 2 \times 2\) binary probability
distributions, \(\Delta\) has 7 dimensions, which split in 5
Dimensions for \(V\) and 2 Dimensions for \(\Delta_p\). This case is also of special interest for evaluating a measure
of partial information decomposition because there are prior
intuitions regarding redundancy and synergy for simple logic
functions.

In the following, \(V\) will be parameterized by the variables
\begin{align}
a = p_Y(0), b &= p_{X_1|Y}(0,0), d = p_{X_2|Y}(0,0) \\ 
            c &= p_{X_1|Y}(0,1), e = p_{X_2|Y}(0,1)  \text{ ,}
\end{align}
and by the coefficients \(\gamma_1,\gamma_2\) of \(\gamma_{0;0;1;0;1},\gamma_{1;0;1;0;1}\).

Table \ref{tab:parameterizations} shows the decomposition \ref{eq:decomposition} of a
general \(2\times 2 \times 2\) probability measure.


\begin{table}[htbp]
\centering
\begin{tabular}{rrrl}
\(Y\) & \(X_1\) & \(X_2\) & \(p(y,x_{1},x_{2})\)\\
0 & 0 & 0 & \(abd + \gamma_1\)\\
0 & 0 & 1 & \(ab(1-d) - \gamma_1\)\\
0 & 1 & 0 & \(a(1-b)d - \gamma_1\)\\
0 & 1 & 1 & \(a(1-b)(1-e) + \gamma_1\)\\
1 & 0 & 0 & \((1-a)ce + \gamma_2\)\\
1 & 0 & 1 & \((1-a)c(1-e) - \gamma_2\)\\
1 & 1 & 0 & \((1-a)(1-c)e - \gamma_2\)\\
1 & 1 & 1 & \((1-a)(1-c)(1-e) + \gamma_2\)\\
\end{tabular}
\caption{Parameterization of \(2\times 2 \times 2\) distributions \label{tab:parameterizations}}

\end{table}

The basis vectors
\(\gamma_{0;0;1;0;1},\gamma_{1;0;1;0;1}\) have distinct support 
which makes  \(\Delta_p\) a rectangle  with allowed parameter range
\begin{align*}
 - \min{\{abd,a(1-b)(1-d)\}} &\leq \gamma_1 \leq \min{\{ab(1-d),a(1-b)d\}}\\
 - \min{\{(1-a)ce,(1-a)(1-c)(1-e)\}} &\leq \gamma_2 \leq \min{\{(1-a)c(1-e),(1-a)(1-c)e\}} \text{ .}
\end{align*}
The lower and upper bounds on \(\gamma_i\) will be denoted by
\(\gamma_{i_{\text{min}}}\) and \(\gamma_{i_{\text{max}}}\) respectively.



The condition for non-uniqueness becomes for the \(2\times 2\times
   2\) decomposition \(b = c \wedge d = e\). For this case, it can
be proven that this is the only condition under which the optimum
is not unique and that the maximizing distribution, if it is
attained in the interior of \(\Delta_p\), can be caracterised by
conditional independence of the output \(Y\) and one input given
the other input. \\

\begin{thm}
Assume that \(p\) has full support and \(\tilde{q} =
   \operatorname{argmax}_{Q\in \Delta_p}{H_Q(Y|X_1,X_2)} \in
   \overset{\circ}{\Delta}_p\). Then, \(\tilde{q}\) is unique if \(b
   \neq c \vee d \neq e\) and it holds that \(\tilde{q}_Y \independent
   \tilde{q}_{X_1} | \tilde{q}_{X_2}\) or \(\tilde{q}_Y \independent
   \tilde{q}_{X_2} | \tilde{q}_{X_1}\).
\end{thm}


\begin{proof}

Under the assumption that the optimum is attained in the interior of
\(\Delta_p\), it is characterized by \(\frac{\partial
   H_q(Y|X_1,X_{2})}{\partial_{\gamma_{y,x_1,x_2}}} = 0\). Rewriting
these equations in terms of conditional probabilities gives
\begin{align}
\log{\frac{\tilde{q}(y|x_1',x_2) \tilde{q}(y|x_1,x_2')}{\tilde{q}(y|x_1^0,x_2^0) \tilde{q}(y|x_1',x_2')}} &= 0  \\
\log{\frac{\tilde{q}(y'|x_1',x_2) \tilde{q}(y'|x_1,x_2')}{\tilde{q}(y'|x_1^0,x_2^0) \tilde{q}(y'|x_1',x_2')}} &= 0 \text{ ,}
\end{align}
where \(Y = \{y,y'\}, X_1 = \{x_1^0,x_1'\}, X_2 = \{x_2^0,x_2' \}\).
Using \(\tilde{q}(y'|x_1,x_2) = 1 - \tilde{q}(y|x_1,x_2)\), these equations reduce to
\begin{align}
\tilde{q}(y|x_1^0,x_2^0) \tilde{q}(y|x_1',x_2') = \tilde{q}(y|x_1',x_2) \tilde{q}(y|x_1,x_2') \\
\tilde{q}(y|x_1^0,x_2^0) + \tilde{q}(y|x_1',x_2') = \tilde{q}(y|x_1',x_2) +\tilde{q}(y|x_1,x_2') \text{ .}
\end{align}
This system has exactly two possible solutions
\begin{align*}
I: \tilde{q}(y|x_1^0,x_2^0) &= \tilde{q}(y|x_1',x_2^0) \wedge \tilde{q}(y|x_1',x_2') = \tilde{q}(y|x_1^0,x_2') \\
II: \tilde{q}(y|x_1^0,x_2^0) &= \tilde{q}(y,x_1^0,x_2') \wedge \tilde{q}(y|x_1',x_2') = \tilde{q}(y|x_1',x_2^0)  \text{ ,}
\end{align*}
where condition \(I\) is equivalent to \(\tilde{q}_Y \independent
   \tilde{q}_{X_1} | \tilde{q}_{X_2}\), while Condition \(II\) is
equivalent to \(\tilde{q}_Y \independent \tilde{q}_{X_2} |
   \tilde{q}_{X_1}\).

Inserting the parameterization given in table
\ref{tab:parameterizations} and using the injectivity of \(\frac{1}{1+x}\) leads for case \(I\) to the
equations \footnote{No solutions exist for which one denominator equals 0. The same applies for case \(II\).}
\begin{align*}
\frac{(1-a)ce + \gamma_2}{abd + \gamma_1} &= \frac{(1-a)(1-c)e - \gamma_2}{a(1-b)d - \gamma_1} \\
\frac{(1-a)c(1-e) - \gamma_2}{ab(1-d) - \gamma_1} &= \frac{(1-a)(1-c)(1-e) + \gamma_2}{a(1-b)(1-d) + \gamma_{1}} \text{ ,}
\end{align*}
which simplify to 
\begin{align*}
\gamma_2 ad - \gamma_1 (1-a)e &= a(1-a)de(b - c) \\
\gamma_1 (1-a)(1-e) - \gamma_2 a(1-d) & =  a(1-a)(1-d)(1-e)(b - c) \text{ .}
\end{align*}
rearranging for \(\gamma_1,\gamma_2\) leads to
\begin{align} \label{eq:case1}
\gamma_1 (d - e) &= ad(b - c)(1 - d) \\
\gamma_2 (d - e) &= e(b-c)(1-a)(1-e) \text{ .}
\end{align}

For  \(d \neq e\), there exists a unique solution. For \(b = c\), the
optimum is \(Q_0\) itself.

Similarly, case \(II\)  reduces to 
\begin{align}
\gamma_2 ab - \gamma_1 (1-a)c &= a(1-a)bc(d-e) \\
\gamma_1 (1-a)(1-c) - \gamma_2 a(1-b) &= a(1-a)(1-b)(1-c)(d-e)
\end{align}
and rearranging for \(\gamma_1,\gamma_2\) gives
\begin{align} \label{eq:case2}
\gamma_1 (b-c) &= ab(d - e)(1 - b) \\
\gamma_2 (b-c) &= c(d - e)(1-a)(1-c) \text{ .}
\end{align}

Again, there exists a unique solution for \(b \neq c\) and \(Q_0\)
is the optimum for \(d = e\).
\end{proof}

It is an open question whether the characterization of the
maximizers in terms of conditional independence generalizes to
higher dimensional Partial Information Decompositions. It is already known that the
conditional independence assumptions are sufficient for being the
maximizer of \(\max_{Q\in \Delta_p}{H_Q(Y|X_1,X_2)}\)
\cite[][ Lemma 13]{bertschinger2014quantifying}, but whether they
are sufficient and whether the uniqueness conditions generalize is
not known.

Only the case where the maximizer lies on the boundary of
\(\Delta_p\) remains to be analyzed. \\



\begin{thm}
Assume that p has full support and that \(\tilde{q} =
   \operatorname{argmax}_{Q\in \Delta_p}{H_Q(Y|X_1,X_2)}\) lies at
the boundary of \(\Delta_p\). Then, it is attained either at
\((\gamma_{1_{\text{min}}},\gamma_{2_{\text{min}}})\) or \((\gamma_{1_{\text{max}}},\gamma_{2_{\text{max}}})\).
\end{thm}

\begin{proof}
If \(\tilde{q}\) lies on the the bottom line \(\gamma_2 =
   \gamma_{2_{\text{min}}}\) \[\frac{\partial
   H_q(Y|X_1,X_{2})}{\partial_{\gamma_2}} =
   \log{\frac{\tilde{q}(y'|x_1',x_2)
   \tilde{q}(y'|x_1,x_2')}{\tilde{q}(y'|x_1^0,x_2^0)
   \tilde{q}(y'|x_1',x_2')}} \leq 0 \text{, }\] and \(\frac{\partial
   H_q(Y|X_1,X_{2})}{\partial_{\gamma_2}} \geq 0\) if \(\tilde{q}\)
lies on the the top line \(\gamma_2 = \gamma_{2_{\text{max}}}\).
The derivatives at the boundary are understood as limits of
derivatives approaching it.

Similarly, 
\[\frac{\partial H_q(Y|X_1,X_{2})}{\partial_{\gamma_1}}
   = \log{\frac{\tilde{q}(y|x_1',x_2)
   \tilde{q}(y|x_1,x_2')}{\tilde{q}(y|x_1^0,x_2^0)
   \tilde{q}(y|x_1',x_2')}} \geq 0\]
if \(\tilde{q}\) lies on
\(\gamma_1 = \gamma_{1_{\text{max}}}\) and \(\frac{\partial
   H_q(Y|X_1,X_{2})}{\partial_{\gamma_1}} \leq 0\) for \(\tilde{q}\)
on \(\gamma_1 = \gamma_{1_{\text{min}}}\). 

The condition \(\lambda_{i} = \lambda_{i_{\text{min}}}\) while
\(\lambda_{j} \neq \lambda_{j_{\text{min}}}\), \(i \neq j\)
implies that one term in the denominator of \(\frac{\partial
   H_q(Y|X_1,X_{2})}{\partial_{\gamma_i}}\) is zero, while
\(\lambda_{i} = \lambda_{i_{\text{max}}}\), while \(\lambda_{j}
   \neq \lambda_{j_{\text{max}}}\), \(i \neq j\) forces a term in the
nominator of \(\frac{\partial
   H_q(Y|X_1,X_{2})}{\partial_{\gamma_i}}\) to be zero (see table
\ref{tab:parameterizations}). A case analysis shows that in these
cases the derivatives point to the interior of \(\Delta_p\). Thus it
is only possible to approach the boundary while maximizing
\(H(Y|X_1,X_2)\) in such a way that no term in the derivatives
tends to zero, which can only be achieved for  
\((\lambda_{1_{\text{min}}},\lambda_{2_{\text{min}}})\) or 
\((\lambda_{1_{\text{max}}},\lambda_{2_{\text{max}}})\).
\end{proof}


These results make it possible to exactly solve
\(\operatorname{argmax}_{Q \in \Delta_p}(H_Q(Y|X_1,X_2)\) by
checking whether the solutions of \eqref{eq:case1}, i.e. \eqref{eq:case2}
lie in the interior of \(\Delta_p\) and otherwise using the maximum of
\(H(Y|X_1,X_2)\) at
\((\gamma_{1_{\text{min}}},\gamma_{2_{\text{min}}})\) and
\((\gamma_{1_{\text{max}}},\gamma_{2_{\text{max}}})\).


Comparisons of the numerical solutions using matlab's
\(\operatorname{fmincon}\) and the analytical solution for
different \(a,b,c,d,e\) are shown in figure \ref{fig:num}.

\begin{figure}[H]
\begin{tabular}{cc}
\includegraphics[width=0.45 \textwidth,height=0.45 \textwidth]{images/20170330_152840_1785HTM.png} & \includegraphics[width=0.45 \textwidth,height=0.45 \textwidth]{images/20170330_153454_1785UdS.png} \\  (a)  & (b)\\
\includegraphics[width=0.45 \textwidth,height=0.45 \textwidth]{images/20170330_153638_1785hnY.png} & \includegraphics[width=0.45 \textwidth,height=0.45 \textwidth]{images/20170330_154633_1785uxe.png}  \\ (c) & (d)
\end{tabular}
\caption{\label{fig:num} Heat plot of \(I(Y|X_1,X_2)\) over \(\Delta_p\) for different coordinates \(a,b,c,d,e\).
The point \((0,0)\) marks \(Q_0\). The path of the numerical optimizer is shown as red line. The cross marks the analytical minimum.
(a) The minimum lies in the interior of \(\Delta_p \). The situation in which the minimum lies on the edge \(\gamma_{1_{\text{min}}},\gamma_{2_{\text{min}}}\) is displayed in (b).
(c) shows the case where \(b=c\) and the minimum is given by \(Q_0 \). In (d), the whole main diagonal minimizes \(I(Y|X_1,X_2)\) and \(b = c \wedge d = e \). }
\end{figure}

\section{Approximative gradient ascend rule for goal functions \label{sec:approx}}
\label{sec:org45e1d0f}


After solving the optimization problem \(\tilde{q}(p) =
  \operatorname{argmax}_{Q \in \Delta_p}H_Q(Y|X_1,X_2)\), the Partial
Information Decomposition is given by classical information
theoretic measures evaluated at \(p\) and \(\tilde{q}(p)\), as can
be seen in equations \eqref{eq:pid-info}-\eqref{eq:pid-info-end}. Thus, both the gradient of
these information theoretic measures, which are known, and the
gradient of \(\tilde{q}\) in dependence of \(p\) influence the
gradient of the unique-, shared- and synergistic information.
Unfortunately, it is not known how \(\tilde{q}\) depends on \(p\) in
the general case. This section introduces an approximation for this
dependence and derives an approximate gradient ascend algortithm for
the Partial Information Decomposition terms.

To formalize this, note that \(\tilde{q}\) is naturally defined on
the quotient space \(\tilde{q}: V \rightarrow
  \Delta\). The goal is to approximate \(\nabla
  H_{\tilde{q}}(Y|X_1,X_2) = \nabla(H(Y|X_1,X_2) \circ \tilde{q})\).
As shown in section \ref{sec:analytical}, \(\tilde{q}\) is not unique
in all cases. Here, only a neighborhood \(U \subset V\) of \(p\) in which \(H(Y|X_1,X_2)_{|\Delta_{p'}}\) is
strongly convex for all \(p'\in V\) is considered. The smooth
dependence of the conditional entropy and the linear constraints on
\(p\) then imply that \(\tilde{q}_U\) is continuously differentiable.


By the chain rule, \(\nabla H_{\tilde{q}} (Y|X_1,X_2) = \nabla
  H_{\tilde{q}(p)}(Y|X_1,X_2) \cdot \nabla_{V} \tilde{q}(p)\).
Neglecting the dependence of \(\tilde{q}\) on p by setting
\(\nabla_V\tilde{q}(p) = \mathds{P}_V\) to the projection to \(V\),
the gradient is approximated by \(\tilde{\nabla}H_{\tilde{q}}(Y|X_1,X_2) := \nabla_V
  H_{\tilde{q}}(Y|X_1,X_2)\).

In order to use this approximation in a gradient ascend algorithm,
the following two timescale algorithm is proposed: 
\begin{enumerate}
\item Write \(G\) in entropic terms
\begin{align}\label{eq:G-entropic}
   G(p) &= \Gamma_0 I^{\text{unq}}_p(Y:X_1 \setminus X_2) +\Gamma_1 I^{\text{unq}}_p(Y:X_2 \setminus X_1) \nonumber{} \\
    &+ \Gamma_2 I^{\text{shd}}_p(Y:X_1;X_2) + \Gamma_3 I^{\text{syn}}_p(Y:X_1;X_2)  \nonumber{}\\
    &+ \Gamma_4 H_p(Y|X_1,X_2) \nonumber{}\\
    &= (\Gamma_0 - \Gamma_2)H_p(Y|X_2) + (\Gamma_1 - \Gamma_2)H_p(Y|X_1) \nonumber{} \\
    &+  \Gamma_2H_p(Y) + (\Gamma_4 - \Gamma_3)H_p(Y|X_1,X_2)\nonumber{} \\ 
    &+ (\Gamma_2 + \Gamma_3 - \Gamma_0 - \Gamma_1)H_{\tilde{q}(p)}(Y|X_1,X_2) \texŧ{ .}
  \end{align}
\item Calculate the gradients of the \(H_p\) terms.
\item Solve the optimization problem to obtain \(\tilde{q}(p)\).
\item Calculate \(\tilde{\nabla}H_{\tilde{q}}(Y|X_1,X_2)\).
\item Move \(p\) in the gradient direction with small learning rate
\(\epsilon\), so that \(p\) stays a probability distribution.
\item Continue until convergence with step 2.
\end{enumerate}

There is an additional complication in step 4. Like in figure
\ref{fig:num} (b), it happens that \(\tilde{q}(p)\) doesn't have full
support even if \(p\) has no zero entries and in this case the
derivative of the conditional entropy isn't defined. This can also
happen when exploiting the continuity property of \(\tilde{q}(p)\).
The optimal starting point would be a Kullback-Leibler projection of
\(\tilde{q}(p^t)\) to \(\Delta_{p^{t+1}}\), however solving this
convex problem to get a better starting point for the convex problem
of maximizing the conditional entropy is numerically too expensive.
An effective approximation is to use the starting point given by the
coefficients of \(p^{t+1}\) in \(V\) and the
coefficients \(\gamma^t\) of \(p^t\) in \(\Delta_{p^t}\),
\(p^{t+1}_{\text{start}} = p^{t+1}_{|V} +
  p^t_{|\Delta_{p^t}}\) and project this distribution to the interior of
\(\Delta_{p^{t+1}}\). The projection is done by shrinking the
coefficients \(\gamma_{y,x_1',x_2'}\) towards 0 by 

\[\gamma^{t+1} = \max_{c\in[0,1]}\left
  \{\min{p^{t+1}_yp^{t+1}_{x_1|y}p^{t+1}_{x_2|y} + c\Gamma \gamma^t}
  \geq \epsilon \right \} \gamma^t \text{ .}\]


\subsection{Learning rule for a binary neuron \label{sec:learning}}
\label{sec:orgb94e987}

This section shows preliminary results that apply this approximative
gradient ascend algorithm to optimize the weights of a model neuron.
The neural architecture is taken from \cite{kay1997activation}. 

The model consists of a binary neuron that has two distinct
integration sites. The receptive field, bottom up sensory inputs
arrive at the integration site \(X_1\) while modulatory input
arrives at \(X_2\). This is a very simple abstraction of pyramidal
cells in layer 4 of the neocortex which physiologically have this
distinct integration sites. Contextual input in this case can
consist of lateral connections, other thalamic inputs etc. For a
review, see \cite{phillips2015functions}.

The net receptive field \(s_r\) and context inputs \(s_c\) are the
weighted sums of the receptive field and contextual connections to the neuron

\[s_r = \sum_{i=1}^N w_i(X_1)_i  \text{, } \hspace{1cm} sc = \sum_{i=1}^N v_i(X_2)_i \]

These two net inputs are treated differently by the activation
function that determines the conditional probability \(\theta\) of firing a
spike, i.e \(Y = 1\) given the net input.

\begin{align*}
    P(Y=1|X_1 &= x_1,X_2 = x_2) = \frac{1}{1 + e^{(-A(s_r(x_1),s_c(x_2)))}} =: \theta\\
    A(s_r,s_c) &= \frac{1}{2}s_r(1 + e^{2s_rs_c})       
\end{align*}

The activation function is mainly determined by the receptive field
net input while \(s_c\) can only modulate the activation. For
motivation of this functional form see \cite{kay1997activation}.

With this conditional approach, learning can be done in an online
or offline modus. For offline learning, the distribution of
\(X_1,X_2\) is estimated from the incoming spikes and used to
optimize the weights \(w,v\) of the neuron. For the biologically
more relevant online learning, running averages for the needed
conditional averages are used at each time step.

The gradient ascent algorithm for \(H(Y),H(Y|X_i),H(Y|X_1,X_2)\) is
derived in \cite{kay1999neural}. Together with the strategy to
approximate \(\nabla H_{\tilde{q}}(Y|X_1,X_2)\), this gives a
gradient ascend rule to optimize the weights of the neuron for the
goal function \(G\), that reduces to the gradient ascend rule of
Coherent Infomax for \(G\) that are expressible in the Coherent
Infomax framework. In particular, from \eqref{eq:G-entropic} it follows
that every function \(G\) with \(\Gamma_{0} + \Gamma_1 \neq
   \Gamma_2 + \Gamma_3\) is not expressible in the Coherent Infomax
framework.

Figure \ref{fig:optim} shows the value of the recently proposed goal
function coding with synergy \cite{wibral2015partial} given by
\begin{align*}
G_{\text{CWS}}(p) =   &I^{\text{unq}}_p(Y:X_1 \setminus X_2) - I^{\text{unq}}_p(Y:X_2 \setminus X_1) \\
                    &+ I^{\text{shd}}_p(Y:X_1;X_2) + I^{\text{syn}}_p(Y:X_1;X_2) - H_p(Y|X_1,X_2) \text{, }
\end{align*}                      
with uniformly chosen random \(10 \times 10\) distribution of
\((X_1,X_2)\) and initial weights \(w,v\) uniformly chosen between
\((-0.1,0.1)\) using offline learning. However, this example is not
very informative. The initial value of nearly \(-1\) is explained
by the \(- H_p(Y|X_1,X_2)\) term in \(G_{\text{CWS}}\) and the
resulting distribution after learning has deterministically \(Y =
   1\) for all \(X_1\) and \(X_2\) values.

More work remains to be done to evaluate \(G_{\text{CWS}}\) and
other goal function on \emph{functionally relevant} scenarios instead of
uniformly chosen distributions \((X_1,X_2)\). These experiments
were done for Coherent Infomax goal functions and are described in
\cite{kay1999neural}. With a specific learning rule, two neurons with
overlapping receptive field that provide each other their output as
context input learn to encode different features of the receptive
field, even if one feature is more informative than the other.
Another open question is whether learning goal functions like
\(G_{\text{CWS}}\) can be done without hard coding the contextual
modulation of \(s_r\) in the activation function \(A(s_r,s_c)\).


\begin{figure}[htbp]
\centering
\includegraphics[width=300px]{/home/kima/Dropbox/uni/labrotation_jost/G_cws_optim.png}
\caption{\label{fig:optim} Optimization of \(G_{cws}\) with randomly generated \(10\times 10\) distribution of \((X_1,X_2)\).}
\end{figure}

\section{Summary}
\label{sec:org0dd2e66}

In this labreport the problem of optimizing a goal function given in
the Partial Information Decomposition and using the measure proposed
by \cite{bertschinger2014quantifying} is considered. Finding a
gradient ascend rule to optimize this goal functions is challenging
because the Partial Information Decomposition itself is defined in
terms of a convex optimiziation problem \(\max_{Q\in \Delta_p}{H_Q(Y|X_1,X_2)\) and changes in the argument
cause different convex spaces for the optimization problem. The
strategy uses continuity assumptions about the maximizing
distribution to derive an approximative learning rule.

This learning rule assumes that the position of the maximizing
distribution \(\tilde{q}(p)\) in the new convex space is given by the projection onto
it applied to the maximizing distribution obtained in the previous
step. To use this rule, a two timescale optimization algorithm was
proposed that uses this approximative gradient and uses the
projection as starting point for the convex optimization problem in
the next time step, so that at each step one relaxes back to the
maximizing distribution.

This strategy depends on the smooth dependence of the maximizing
distribution \(\tilde{q}(p)\) on \(p\), and thus needs uniquenes
\(\tilde{q}(p)\). Also, if one could analytically solve the
optimization problem one could specify the dependence of \(\tilde{q}\) on \(p\) and could calculate the real gradient for the goal
functions.

This motivated further analytical study of the convex optimization
problem. A degeneracy condition, that the output \(Y\) is
independent of the input \(X_1\) as well as \(X_2\), could be
derived in which the optimization problem has no unique solution.

The special case of Partial Information Decomposition of binary
distributions proved to be analytically tractable and it could be
shown that this degeneracy condition is the only condition for
non-uniqueness of the optimization problem and that solutions in the
interior of \(\Delta_p\) are attained at a distribution where the
output is conditionally independent of one input given the other.
Furthermore, \(\tilde{q}\) can be calculated exactly in this
special case. It is an open question whether this classification can
be extended to higher dimensional cases. 

A proof of concept application of the approximative gradient
learning rule to a neuron model studied in the Coherent Infomax
framework was provided, but more work remains to be done to study
the effects of information processing that optimizes a Partial
Information Decomposition goal function in computer simulations of
neural architectures.

\newpage

\nocite{williams2010nonnegative}
\nocite{wibral2015partial}
\nocite{bertschinger2014quantifying}
\nocite{phillips2015functions}
\nocite{kay1997activation}
\nocite{kay1999neural}
\nocite{kay1998contextually}

\addcontentsline{toc}{section}{References}   
\bibliographystyle{plainnat}
\bibliography{../../emacs/bibliography/references}
\end{document}