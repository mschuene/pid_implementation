function [H_Y] = calc_HY(P_YXZ)
    P_Y = squeeze(sum(sum(P_YXZ,3),2));
    H_Y = -nansum(P_Y.*log2(P_Y));
end