function [ dzjgxi ] = dpdzjgxi(pxyz,j,i,px,pygx,~)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    [~,~,cardz] = size(pxyz);
    if nargin < 4
        [px,pygx,~] = dpcoords(pxyz);
    end
    dzjgxi = @(x,y,z) inner(px,pygx,j,i,cardz,x,y,z);

end

function d = inner(px,pygx,j,i,cardz,x,y,z)
    if i == x && j == z
        d = px(i).*pygx(i,y);
    elseif i == x && z == cardz
        d = -px(i).*pygx(i,y);
    else
        d = 0;
    end
end