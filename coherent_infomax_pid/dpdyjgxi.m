function [ dyjgxi ] = dpdyjgxi(pxyz,j,i,px,~,pzgx)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    [~,cardy,~] = size(pxyz);
    if nargin < 4
        [px,~,pzgx] = dpcoords(pxyz);
    end
    dyjgxi = @(x,y,z) inner(px,pzgx,j,i,cardy,x,y,z);

end

function d = inner(px,pzgx,j,i,cardy,x,y,z)
    if i == x && j == y
        d = px(i).*pzgx(i,z);
    elseif i == x && y == cardy
        d = -px(i).*pzgx(i,z);
    else
        d = 0;
    end
end