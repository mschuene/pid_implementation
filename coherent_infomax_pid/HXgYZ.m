function [ H ] = HXgYZ(pxyz,pxgyz)
%HXGYZ Summary of this function goes here
%   Detailed explanation goes here
    [cardx,~,~] = size(pxyz);
    if nargin < 2
        pyz = repmat(sum(pxyz,1),[cardx,1,1]);
        pxgyz = pxyz./pyz;
        pxgyz(isnan(pxgyz)) = 0;
    end
    H = pxyz.*log2(pxgyz);
    H(isnan(H)) = 0;
    H = -sum(H(:));
end

