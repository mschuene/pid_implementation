close all;
dimx = 2;
dimz = 2;
cardx = 2;
cardz = 2;
w = 0.1*rand(dimx,1);v=0.1*rand(dimz,1);
X_values=rand(cardx,dimx);%[0.5,0;1,2];
Z_values=rand(cardz,dimz);%[2,1/3;1,3];
P_XZ = squeeze(generate_sample_dist(1,cardx,cardz));
wv = [w;v];
wvs = [wv];
fvals = [];
[A,dAdr,dAdc] = activation_function(0.5,2);
coef = -1;
fvals2 = [];
h = animatedline(gca,'Marker','o');
% Optimize H(Y|XZ)
for i = 1:4000
    i
    P_YXZ = construct_dist(@(a,b,c,d)single_neuron(a,b,c,d,A),w,v,X_values,Z_values,P_XZ);
    fval = calc_HY_X(P_YXZ);
    %calculate gradient
    [X,Z] = ndgrid(1:cardx,1:cardz);
    x = reshape(X_values(X,:),[cardx,cardz,size(w)]);
    c = reshape(Z_values(Z,:),[cardx,cardz,size(v)]);
    sr = reshape(w'*X_values(X,:)',size(X))
    sc = reshape(v'*Z_values(Z,:)',size(Z))
    theta = 1./(1 + exp(-A(sr,sc)))%squeeze(P_YXZ(2,:,:)./sum(P_YXZ,1))
    P_XZ = squeeze(sum(P_YXZ,1));
    %E_x = squeeze(sum(P_YXZ(2,:,:),2));
    P_YX = squeeze(sum(P_YXZ,3));
    P_YgX = P_YX./repmat(sum(P_YX,1),[2,1]);
    E_x = P_YgX(2,:);
    dH = - (log2(E_x(X)) - log2(1-E_x(X))) .* theta .* (1 - theta);
    dw = squeeze(nansum(nansum(bsxfun(@times,P_XZ .* dH .* dAdr(sr,sc),x),2),1));
    dv = squeeze(nansum(nansum(bsxfun(@times,P_XZ .* dH .* dAdc(sr,sc),c),2),1));
    if(isnan(fval)) || numel(find(isnan([dw;dv]))) > 0
        break
    end
    w = w + 0.1.*coef.*dw;
    v = v + 0.1.*coef.*dv;
%     fval2 = -nansum(nansum(P_XZ .* (theta .* log2(theta) + (1-theta).*log2(1-theta)),1),2);
%     fvals2 = [fvals2,fval2];
    if numel(fvals) > 0 && sign(fval - fvals(end)) ~= sign(coef)
        r = 1;
    end
    fvals = [fvals,fval];
    addpoints(h,i,fval);
    drawnow;
end
