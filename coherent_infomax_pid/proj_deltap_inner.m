function [Q_inner,xorig_new] = proj_deltap_inner(Q,GB,x_orig)
%UNTITLED2 Q return from optimizer
%   GB xorig Basis and coefficients inside delta_p 

    delta = GB*x_orig;
    delta = - delta./norm(delta);
    Qv = Q(:);
    c = max([0;-Qv(Qv <= 0)./delta(Qv <= 0)]);
    if(isnan(delta))
        Q_inner = Q;
        xorig_new = x_orig;
    else
        Q_inner = reshape(Q(:) + (c + 1e-6)*delta,size(Q));
        xorig_new = (1 - (c - 1e-6)/norm(GB*x_orig))*x_orig;
    end
end

