function [HY_Z] = calc_HY_Z(P_YXZ)
 P_YZ = squeeze(sum(P_YXZ,2));
 P_Z = repmat(sum(P_YZ,1),[size(P_YZ,1),1]);
 HY_Z = P_YZ .* (log2(P_YZ) - log2(P_Z));
 HY_Z = -nansum(HY_Z(:));
end