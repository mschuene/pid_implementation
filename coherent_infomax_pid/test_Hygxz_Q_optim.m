close all;
dimx = 10;
dimz = 10;
cardx = 10;
cardz = 10;
w = 0.1*rand(dimx,1);v=0.1*rand(dimz,1);
cardw = dimx;cardv = dimz;
X_values=rand(cardx,dimx);%[0.5,0;1,2];
Z_values=rand(cardz,dimz);%[2,1/3;1,3];
P_XZ = squeeze(generate_sample_dist(1,cardx,cardz));
wv = [w;v];
wvs = [wv];
fvals = [];
[A,dAdr,dAdc] = activation_function(0.5,2);
coef = 1;
fvals2 = [];
GB = create_gamma_basis(1,1,2,cardx,cardz);
x_orig = zeros(size(GB,2),1);
algorithm = 'interior-point';
h = animatedline(gca,'Marker','o');
% Optimize H(Y|XZ)
for i = 1:4000
    i
    P_YXZ = construct_dist(@(a,b,c,d)single_neuron(a,b,c,d,A),w,v,X_values,Z_values,P_XZ);
    [py,pxgy,pzgy] = dpcoords(P_YXZ);
    P_YXZ_start = starting_dist(py(1:(end-1)),pxgy(:,1:(end-1)),pzgy(:,1:(end-1)));
    [Pnew_inner,x_orig_start] = proj_deltap_inner(reshape(P_YXZ_start(:) + GB*x_orig,size(P_YXZ_start)),GB,x_orig);
    [UIX,UIZ,SI,CI,Q,fval,exitflag,output,lambda,grad,hessian,x_orig,HISTORY] = PID(P_YXZ_start,algorithm,x_orig_start);
    disp('Q returned ');
    Q;
    [Q,x_orig] = proj_deltap_inner(Q,GB,x_orig);
    fval = calc_HY_XZ(Q)
    %calculate gradient
    [X,Z] = ndgrid(1:cardx,1:cardz);
    x = reshape(X_values(X,:),[cardx,cardz,size(w)]);
    c = reshape(Z_values(Z,:),[cardx,cardz,size(v)]);
    sr = reshape(w'*X_values(X,:)',size(X));
    sc = reshape(v'*Z_values(Z,:)',size(Z));
%     
%     theta = 1./(1 + exp(-A(sr,sc)));%squeeze(P_YXZ(2,:,:)./sum(P_YXZ,1))
%     Q_XZ = squeeze(sum(Q,1));
%     theta_star = squeeze(Q(2,:,:)./sum(Q,1));
%     lts = log2(theta_star) - log2(1 - theta_star);
%     dG_Q = - lts .* theta .* (1 - theta);
%     dGdw_Q = bsxfun(@times,Q_XZ .* dG_Q .* dAdr(sr,sc),x);
%     dGdv_Q = bsxfun(@times,Q_XZ .* dG_Q .* dAdc(sr,sc),c);
%     dGdw_Q = squeeze(nansum(nansum(dGdw_Q,2),1));
%     dGdv_Q = squeeze(nansum(nansum(dGdv_Q,2),1));
%     dGdw_Q(abs(dGdw_Q)>1000) = sign(dGdw_Q(abs(dGdw_Q)>100))*1000
%     dGdv_Q(abs(dGdv_Q)>1000) = sign(dGdv_Q(abs(dGdv_Q)>100))*1000
%    
%     w = w + 0.1 .* coef .* dGdw_Q;
%     v = v + 0.1 .* coef .* dGdv_Q;
    
    [dhdy,dhdxgy,dhdzgy] = dhddeltapo(Q,py,pxgy,pzgy); 
    Q_XZ = squeeze(sum(Q,1));
    theta_star = squeeze(Q(2,:,:))./Q_XZ;
    ts = theta_star.*(1 - theta_star);
    tsrx = bsxfun(@times,ts.*dAdr(sr,sc),x); %cardx*cardz*cardw
    tscz = bsxfun(@times,ts.*dAdc(sr,sc),c);

    dy1dw = bsxfun(@times,Q_XZ,tsrx);
    dy1dw = squeeze(sum(sum(dy1dw,2),1)); %size cardw
    dy1dv = bsxfun(@times,Q_XZ,tscz);
    dy1dv = squeeze(sum(sum(dy1dv,2),1));
    dy0dw = -dy1dw; % y0 = 1 - y1
    dy0dv = -dy1dv;
    
      %Q_Y = squeeze(sum(sum(Q,3),2));
    Q_Z = repmat(sum(Q_XZ,1),[cardx,1]);
    Q_X = repmat(sum(Q_XZ,2),[1,cardz]);
    tsrx_z = squeeze(sum(bsxfun(@times,Q_Z,tsrx),2)); %size [cardx,cardw]
    tscz_z = squeeze(sum(bsxfun(@times,Q_Z,tscz),2)); %size [cardx,cardv]

    tsrx_x = squeeze(sum(bsxfun(@times,Q_X,tsrx),1)); %size [cardz,cardw]
    tscz_x = squeeze(sum(bsxfun(@times,Q_X,tscz),1)); %size [cardz,cardv]
    
    qx = squeeze(Q_X(:,1));
    qz = squeeze(Q_Z(1,:)');

    dxgy1dw = repmat(qx./((1 - py(1))^2),[1,cardw]) .* (tsrx_z .* (1-py(1)) - repmat(dy1dw',[cardx,1]).*repmat((1-py(1)).*pxgy(2,:)',[1,cardw])); %size cardx*cardw
    dxgy0dw = -repmat(qx./(py(1))^2,[1,cardw])      .* (tsrx_z .* py(1)     - repmat(dy1dw',[cardx,1]).*repmat(py(1).*pxgy(1,:)',[1,cardw])); %size cardx*cardw

    dxgy1dv = repmat(qx./((1 - py(1))^2),[1,cardv]) .* (tscz_z .* (1-py(1)) - repmat(dy1dv',[cardx,1]).*repmat((1-py(1)).*pxgy(2,:)',[1,cardv])); %size cardx*cardv
    dxgy0dv = -repmat(qx./(py(1))^2,[1,cardv])      .* (tscz_z .* py(1)     - repmat(dy1dv',[cardx,1]).*repmat(py(1).*pxgy(1,:)',[1,cardv])); %size cardx*cardv

    dzgy1dw = repmat(qz./((1 - py(1))^2),[1,cardw]) .* (tsrx_x .* (1-py(1)) - repmat(dy1dw',[cardz,1]).*repmat((1-py(1)).*pzgy(2,:)',[1,cardw])); %size cardx*cardw
    dzgy0dw = -repmat(qz./(py(1))^2,[1,cardw])      .* (tsrx_x .* py(1)     - repmat(dy1dw',[cardz,1]).*repmat(py(1).*pzgy(1,:)',[1,cardw])); %size cardx*cardw

    dzgy1dv = repmat(qz./((1 - py(1))^2),[1,cardv]) .* (tscz_x .* (1-py(1)) - repmat(dy1dv',[cardz,1]).*repmat((1-py(1)).*pzgy(2,:)',[1,cardv])); %size cardx*cardv
    dzgy0dv = -repmat(qz./(py(1))^2,[1,cardv])      .* (tscz_x .* py(1)     - repmat(dy1dv',[cardz,1]).*repmat(py(1).*pzgy(1,:)',[1,cardv])); %size cardx*cardv

    dhdw = dy0dw * dhdy + ...
           squeeze(nansum(nansum(bsxfun(@times,[reshape(dxgy0dw(1:(end-1),:),[1,cardx-1,cardw]);...
                                                reshape(dxgy1dw(1:(end-1),:),[1,cardx-1,cardw])],dhdxgy),2),1)) + ...
           squeeze(nansum(nansum(bsxfun(@times,[reshape(dzgy0dw(1:(end-1),:),[1,cardz-1,cardw]);...
                                                reshape(dzgy1dw(1:(end-1),:),[1,cardz-1,cardw])],dhdzgy),2),1));
    dhdv = dy0dv * dhdy + ...
           squeeze(nansum(nansum(bsxfun(@times,[reshape(dxgy0dv(1:(end-1),:),[1,cardx-1,cardv]);...
                                                reshape(dxgy1dv(1:(end-1),:),[1,cardx-1,cardv])],dhdxgy),2),1)) + ...
           squeeze(nansum(nansum(bsxfun(@times,[reshape(dzgy0dv(1:(end-1),:),[1,cardz-1,cardv]);...
                                                reshape(dzgy1dv(1:(end-1),:),[1,cardz-1,cardv])],dhdzgy),2),1));
                                      
     w = w + 0.1 .* coef .* dhdw;
     v = v + 0.1 .* coef .* dhdv;

  
    if(isnan(fval)) || numel(find(isnan([dhdw;dhdv]))) > 0
        break
    end
     if numel(fvals) > 0 && sign(fval - fvals(end)) ~= sign(coef)
        r = 1;
    end
    fvals = [fvals,fval];
    addpoints(h,i,fval);
    drawnow;
end
