close all;
dimx = 2; dimz = 2;
cardw = dimx; cardv = dimz;
cardx = 2; cardz = 2;
w = 0.01*rand(dimx,1);v=0.01*rand(dimz,1);
X_values=rand(cardx,dimx);%[0.5,0;1,2];
Z_values=rand(cardz,dimz);%[2,1/3;1,3];

P_XZ = squeeze(generate_sample_dist(1,cardx,cardz));

%gammas = 0.5-rand(1,5);
gammas = [1,-1,1,1,-1];
G_UIX = gammas(1);G_UIZ = gammas(2);G_SI = gammas(3);G_CI = gammas(4); G_H = gammas(5);
gammas = [1,-1,1,1,-1]
wv = [w;v];
wvs = [wv];
fvals = [];
[A,dAdr,dAdc] = activation_function(0.5,2);
fvals2 = [];
GB = create_gamma_basis(1,1,2,cardx,cardz);
x_orig = zeros(size(GB,2),1);
algorithm = 'sqp';
figure(1)
h = animatedline(gca,'Marker','o');
title('G during Optimization');
figure(2)
h2 = animatedline(gca,'Marker','o');
title('first two components of coordinates inside delta_p');
epsilon = 0.1;
method = 'orthogonal';
%%
for i = 1:100000
    i
    % construct new distribution and calculate it's PId distribution
    P_YXZ = construct_dist(@(a,b,c,d)single_neuron(a,b,c,d,A),w,v,X_values,Z_values,P_XZ);
    [py,pxgy,pzgy] = dpcoords(P_YXZ);
    P_YXZ_start = starting_dist(py(1:(end-1)),pxgy(:,1:(end-1)),pzgy(:,1:(end-1)));
    [Pnew_inner,x_orig_start] = proj_deltap_inner(reshape(P_YXZ_start(:) + GB*x_orig,size(P_YXZ_start)),GB,x_orig);
    x_orig_start;   
    [UIX,UIZ,SI,CI,Q,fval,exitflag,output,lambda,grad,hessian,x_orig,HISTORY] = PID(P_YXZ_start,algorithm,x_orig_start);

    [Q,x_orig] = proj_deltap_inner(Q,GB,x_orig);
    PID_terms = [UIX;UIZ;SI;CI;calc_HY_XZ(P_YXZ)];
    fval = gammas*PID_terms;
    
    % calculating auxilliary variables for gradient
    [X,Z] = ndgrid(1:cardx,1:cardz);
    sr = reshape(w'*X_values(X,:)',size(X));
    sc = reshape(v'*Z_values(Z,:)',size(Z));
    theta = 1./(1 + exp(-A(sr,sc)));%squeeze(P_YXZ(2,:,:)./sum(P_YXZ,1))
    x = reshape(X_values(X,:),[cardx,cardz,size(w)]);
    c = reshape(Z_values(Z,:),[cardx,cardz,size(v)]);
    P_XZ = squeeze(sum(P_YXZ,1));
    
    P_YX = squeeze(sum(P_YXZ,3));
    P_YgX = P_YX./repmat(sum(P_YX,1),[2,1]);
    E_x = P_YgX(2,:);
    E_x(isnan(E_x)) = 0;
    
    P_YZ = squeeze(sum(P_YXZ,2));
    P_YgZ = P_YZ./repmat(sum(P_YZ,1),[2,1]);
    E_z = P_YgZ(2,:);
    E_z(isnan(E_z)) = 0;
    
    P_Y = repmat(sum(sum(P_YXZ,3),2),[1,cardx,cardz]);
    E = squeeze(P_Y(2,:,:));
    
    %common part of the gradient for terms that are evaluated at P
    dG = -( (gammas(1) - gammas(3)).*(log2(E_z(Z)) - log2(1 - E_z(Z))) ...
            +(gammas(2) - gammas(3)).*(log2(E_x(X)) - log2(1 - E_x(X)))...
            + gammas(3)             .*(log2(E) - log2(1 - E))          ...
            +(gammas(5) - gammas(4)).*(log2(theta) - log2(1 - theta)))  ...   %A(sr,sc) .* log2(exp(1)))                        ...
           .* theta .* (1 - theta);
    % gradient terms wrt w and v 
    dGdw = squeeze(nansum(nansum(bsxfun(@times,P_XZ .* dG .* dAdr(sr,sc),x),2),1));
    dGdv = squeeze(nansum(nansum(bsxfun(@times,P_XZ .* dG .* dAdc(sr,sc),c),2),1));
    
    gamma_Q = gammas*[-1;-1;1;1;0];
            % part of the gradient that is evaluated at Q
    if abs(gamma_Q) > 1e-8
       if strcmp(method,'orthogonal')
            [dhdy,dhdxgy,dhdzgy] = dhddeltapo(Q,py,pxgy,pzgy); 
            Q_XZ = squeeze(sum(Q,1));
            theta_star = squeeze(Q(2,:,:))./Q_XZ;
            ts = theta_star.*(1 - theta_star);
            tsrx = bsxfun(@times,ts.*dAdr(sr,sc),x); %cardx*cardz*cardw
            tscz = bsxfun(@times,ts.*dAdc(sr,sc),c);

            dy1dw = bsxfun(@times,Q_XZ,tsrx);
            dy1dw = squeeze(sum(sum(dy1dw,2),1)); %size cardw
            dy1dv = bsxfun(@times,Q_XZ,tscz);
            dy1dv = squeeze(sum(sum(dy1dv,2),1));
            dy0dw = -dy1dw; % y0 = 1 - y1
            dy0dv = -dy1dv;

            %Q_Y = squeeze(sum(sum(Q,3),2));
            Q_Z = repmat(sum(Q_XZ,1),[cardx,1]);
            Q_X = repmat(sum(Q_XZ,2),[1,cardz]);
            tsrx_z = squeeze(sum(bsxfun(@times,Q_Z,tsrx),2)); %size [cardx,cardw]
            tscz_z = squeeze(sum(bsxfun(@times,Q_Z,tscz),2)); %size [cardx,cardv]

            tsrx_x = squeeze(sum(bsxfun(@times,Q_X,tsrx),1)); %size [cardz,cardw]
            tscz_x = squeeze(sum(bsxfun(@times,Q_X,tscz),1)); %size [cardz,cardv]

            qx = squeeze(Q_X(:,1));
            qz = squeeze(Q_Z(1,:)');

            dxgy1dw = repmat(qx./((1 - py(1))^2),[1,cardw]) .* (tsrx_z .* (1-py(1)) - repmat(dy1dw',[cardx,1]).*repmat((1-py(1)).*pxgy(2,:)',[1,cardw])); %size cardx*cardw
            dxgy0dw = -repmat(qx./(py(1))^2,[1,cardw])      .* (tsrx_z .* py(1)     - repmat(dy1dw',[cardx,1]).*repmat(py(1).*pxgy(1,:)',[1,cardw])); %size cardx*cardw

            dxgy1dv = repmat(qx./((1 - py(1))^2),[1,cardv]) .* (tscz_z .* (1-py(1)) - repmat(dy1dv',[cardx,1]).*repmat((1-py(1)).*pxgy(2,:)',[1,cardv])); %size cardx*cardv
            dxgy0dv = -repmat(qx./(py(1))^2,[1,cardv])      .* (tscz_z .* py(1)     - repmat(dy1dv',[cardx,1]).*repmat(py(1).*pxgy(1,:)',[1,cardv])); %size cardx*cardv

            dzgy1dw = repmat(qz./((1 - py(1))^2),[1,cardw]) .* (tsrx_x .* (1-py(1)) - repmat(dy1dw',[cardz,1]).*repmat((1-py(1)).*pzgy(2,:)',[1,cardw])); %size cardx*cardw
            dzgy0dw = -repmat(qz./(py(1))^2,[1,cardw])      .* (tsrx_x .* py(1)     - repmat(dy1dw',[cardz,1]).*repmat(py(1).*pzgy(1,:)',[1,cardw])); %size cardx*cardw

            dzgy1dv = repmat(qz./((1 - py(1))^2),[1,cardv]) .* (tscz_x .* (1-py(1)) - repmat(dy1dv',[cardz,1]).*repmat((1-py(1)).*pzgy(2,:)',[1,cardv])); %size cardx*cardv
            dzgy0dv = -repmat(qz./(py(1))^2,[1,cardv])      .* (tscz_x .* py(1)     - repmat(dy1dv',[cardz,1]).*repmat(py(1).*pzgy(1,:)',[1,cardv])); %size cardx*cardv

            dhdw = dy0dw * dhdy + ...
                   squeeze(nansum(nansum(bsxfun(@times,[reshape(dxgy0dw(1:(end-1),:),[1,cardx-1,cardw]);...
                                                        reshape(dxgy1dw(1:(end-1),:),[1,cardx-1,cardw])],dhdxgy),2),1)) + ...
                   squeeze(nansum(nansum(bsxfun(@times,[reshape(dzgy0dw(1:(end-1),:),[1,cardz-1,cardw]);...
                                                        reshape(dzgy1dw(1:(end-1),:),[1,cardz-1,cardw])],dhdzgy),2),1));
            dhdv = dy0dv * dhdy + ...
                   squeeze(nansum(nansum(bsxfun(@times,[reshape(dxgy0dv(1:(end-1),:),[1,cardx-1,cardv]);...
                                                        reshape(dxgy1dv(1:(end-1),:),[1,cardx-1,cardv])],dhdxgy),2),1)) + ...
                   squeeze(nansum(nansum(bsxfun(@times,[reshape(dzgy0dv(1:(end-1),:),[1,cardz-1,cardv]);...
                                                        reshape(dzgy1dv(1:(end-1),:),[1,cardz-1,cardv])],dhdzgy),2),1));

            dGdw = dGdw + gamma_Q .* dhdw;
            dGdv = dGdv + gamma_Q .* dhdv;
       else 
            Q_XZ = squeeze(sum(Q,1));
            theta_star = squeeze(Q(2,:,:)./sum(Q,1));
            lts = log2(theta_star) - log2(1 - theta_star);
            dG_Q = - lts .* theta .* (1 - theta);
            dGdw_Q = bsxfun(@times,Q_XZ .* dG_Q .* dAdr(sr,sc),x);
            dGdv_Q = bsxfun(@times,Q_XZ .* dG_Q .* dAdc(sr,sc),c);
            dGdw_Q = squeeze(nansum(nansum(dGdw_Q,2),1));
            dGdv_Q = squeeze(nansum(nansum(dGdv_Q,2),1));
            dGdw_Q(abs(dGdw_Q)>1000) = sign(dGdw_Q(abs(dGdw_Q)>100))*1000
            dGdv_Q(abs(dGdv_Q)>1000) = sign(dGdv_Q(abs(dGdv_Q)>100))*1000
   

            dGdw = dGdw + gamma_Q .* dGdw_Q;
            dGdv = dGdv + gamma_Q .* dGdv_Q;
       end

    end
    
    % update weights and draw line
    w = w + epsilon*dGdw;
    v = v + epsilon*dGdv;
    
    if(isnan(fval)) || numel(find(isnan([dGdw;dGdv]))) > 0
        break
    end
    
    fvals = [fvals,fval];
    
    fval2 =   (gammas(1) - gammas(3)).*calc_HY_Z(P_YXZ)  ...
            + (gammas(2) - gammas(3)).*calc_HY_X(P_YXZ)  ...
            + gammas(3)              .*calc_HY(P_YXZ)    ...
            +(gammas(5) - gammas(4)) .*calc_HY_XZ(P_YXZ) ...
            + gamma_Q                .*calc_HY_XZ(Q);
    fvals2 = [fvals2,fval2];
    addpoints(h,i,fval2);
    addpoints(h2,x_orig(1),x_orig(2));
    drawnow;
    
end
    