function [P_YXZ] = construct_dist(output_func,w,v,X_values,Z_values,P_XZ)
    P_YXZ = zeros([2,size(P_XZ)]);
    for x = 1:size(P_XZ,1)
        for z = 1:size(P_XZ,2)
            P_YXZ(2,x,z) = output_func(w,v,X_values(x,:),Z_values(z,:));
        end
    end
    P_YXZ(1,:,:) = 1 - P_YXZ(2,:,:);
    P_YXZ(1,:,:) = squeeze(P_YXZ(1,:,:)) .*P_XZ;
    P_YXZ(2,:,:) = squeeze(P_YXZ(2,:,:)) .*P_XZ;
end