function [HY_X] = calc_HY_X(P_YXZ)
 P_YX = squeeze(sum(P_YXZ,3));
 P_X = repmat(sum(P_YX,1),[size(P_YX,1),1]);
 HY_X = P_YX .* (log2(P_YX) - log2(P_X));
 HY_X = -nansum(HY_X(:));
end