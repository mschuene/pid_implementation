function [A,dAdr,dAdc] = activation_function(k1,k2)
    % TODO add derivative here
    A = @(r,c) r.*(k1 + (1 - k1).*exp(k2.*r.*c));
    if nargout > 1
        dAdr = @(r,c) min(k1 + (1-k1).*(1+k2.*r.*c).*exp(k2.*r.*c),10000);
        dAdc = @(r,c) min((1-k1).*k2.*r.^2.*exp(k2.*r.*c),10000);
    end
end
