function [dhdx,dhdygx,dhdzgx] = dhddeltapo(pxyz,px,pygx,pzgx)
%DHDDELTAPO Calculates derivaties of HX_YZ in direction of the coordinates
% of delta_p ('orthogonal' to delta_p)
%   Detailed explanation goes here

    [cardx,cardy,cardz] = size(pxyz);
    if nargin < 2
        [px,pygx,pzgx] = dpcoords(pxyz);
    end
    pxgyz = pxyz./repmat(sum(pxyz,1),[cardx,1,1]);
    pxgyz(isnan(pxgyz)) = 0;
    [x,y,z] = ndgrid(1:cardx,1:cardy,1:cardz);
    xyz = [x(:),y(:),z(:)];
    
    dhdx = zeros(cardx-1,1);
    lp1 = (log2(pxgyz) + 1);
    idx = abs(lp1) >= 10000;
    lp1(idx) = sign(lp1(idx))*10000;
    
    for i = 1:(cardx - 1)
        dxi = dpdxi(pxyz,i,px,pygx,pzgx);
        dxi_xyz = reshape(arrayfun(@(x,y,z)dxi(x,y,z),xyz(:,1),xyz(:,2),xyz(:,3)),[cardx,cardy,cardz]);
        dxi_yz = repmat(sum(dxi_xyz,1),[cardx,1,1]);
        dh = dxi_xyz.*lp1 + pxgyz.*dxi_yz;
        dh(isnan(dh)) = 0;
        dhdx(i) = -sum(dh(:));
    end
    
    dhdygx = zeros(cardx,cardy-1);
    for i = 1:cardx
        for j = 1:(cardy-1)
            dyjxi = dpdyjgxi(pxyz,j,i,px,pygx,pzgx);
            dyjxi_xyz = reshape(arrayfun(@(x,y,z)dyjxi(x,y,z),xyz(:,1),xyz(:,2),xyz(:,3)),[cardx,cardy,cardz]);
            dyjxi_yz = repmat(sum(dyjxi_xyz,1),[cardx,1,1]);
            dh = dyjxi_xyz.*lp1 + pxgyz.*dyjxi_yz;
            dh(isnan(dh)) = 0;
            dhdygx(i,j) = -sum(dh(:));
        end
    end
    
    dhdzgx = zeros(cardx,cardz-1);
    for i = 1:cardx
        for j = 1:(cardz-1)
            dzjxi = dpdzjgxi(pxyz,j,i,px,pygx,pzgx);
            dzjxi_xyz = reshape(arrayfun(@(x,y,z)dzjxi(x,y,z),xyz(:,1),xyz(:,2),xyz(:,3)),[cardx,cardy,cardz]);
            dzjxi_yz = repmat(sum(dzjxi_xyz,1),[cardx,1,1]);
            dh = dzjxi_xyz.*lp1 + pxgyz.*dzjxi_yz;
            dh(isnan(dh)) = 0;
            dhdzgx(i,j) = -sum(dh(:));
        end
    end
end

