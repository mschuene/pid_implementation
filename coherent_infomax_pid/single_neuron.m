function [output]  = single_neuron(w,v,RF,CF,A)
%SUMMARY output of single neuron with parameters w and v and input RF,CF
    if nargin < 5
        k1 = 0.5;k2 = 2;
        A = activation_function(k1,k2);
    end
    sr = w'*RF';
    sc = v'*CF';
    k1 = 0.5;k2 = 2;
    output = 1/(1 + exp(-A(sr,sc)));