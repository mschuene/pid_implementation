function [G,dG,x_orig] = PID_goal_function(wv,P_XZ,X_values,Z_values,G_UIX,G_UIZ,G_SI,G_CI,G_H,x_orig,algorithm)
%UNTITLED2 Evaluates and calculates derivative of PID_goal_function given
%the current weights of the neuron and the distribution of RF(Z) and
%context(X)

    % construct joint distribution needed for PID
    w = wv(1:size(X_values,2))
    v = wv(size(X_values,2) + 1:end)
    [A,dAdr,dAdc] = activation_function(0.5,2);
    P_YXZ = construct_dist(@(a,b,c,d)single_neuron(a,b,c,d,A),w,v,X_values,Z_values,P_XZ);
    [py,pxgy,pzgy] = dpcoords(P_YXZ);
    [cardx,cardz] = size(P_XZ);
    GB = create_gamma_basis(1,1,2,cardx,cardz);
    P_YXZ_start = starting_dist(py(1:(end-1)),pxgy(:,1:(end-1)),pzgy(:,1:(end-1)));
    [Pnew_inner,x_orig_start] = proj_deltap_inner(reshape(P_YXZ_start(:) + GB*x_orig,size(P_YXZ_start)),GB,x_orig);
    [UIX,UIZ,SI,CI,Q,fval,exitflag,output,lambda,grad,hessian,x_orig,HISTORY] = PID(P_YXZ_start,algorithm,x_orig_start);
    HY_XZ = calc_HY_XZ(P_YXZ);
    gammas = [G_UIX,G_UIZ,G_SI,G_CI,G_H]
    G = gammas*[UIX;UIZ;SI;CI;HY_XZ];
    if nargout > 1
        [Q,xorig_new] = proj_deltap_inner(Q,GB,x_orig)
        [dGdw,dGdv] = calc_dG(P_YXZ,Q,A,dAdr,dAdc,w,v,X_values,Z_values,gammas,py,pxgy,pzgy);
        dG = [dGdw;dGdv];
    end
end

