function [ dxi ] = dpdxi(pxyz,i,~,pygx,pzgx)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    [cardx,~,~] = size(pxyz);
    if nargin < 3
        [~,pygx,pzgx] = dpcoords(pxyz);
    end
    dxi = @(x,y,z) inner(pygx,pzgx,i,cardx,x,y,z);

    
end

function d = inner(pygx,pzgx,i,cardx,x,y,z)
    if x == i
        d = pygx(i,y).*pzgx(i,z);
    elseif x == cardx
        d = -pygx(cardx,y).*pzgx(cardx,z);
    else
        d = 0;
    end
end