function [x,fval,exitflag,output,lambda,grad,hessian,x_orig,HISTORY] = Min_MI_X_YZ_gamma(p,algorithm,y0,z0,initial_value)
%MIN_MI_X_YZ_GAMMA Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 2
        algorithm = 'interior-point';
    end
    if nargin < 3
        y0 = 1; z0 = 1;
    end   
    [cardx,cardy,cardz] = size(p);
    [gb,igb] = create_gamma_basis(y0,z0,cardx,cardy,cardz);
    % remaining conditions in the new basis are just the bounds which now are
    % linear qualities
    [ condleq_lhs,condleq_rhs ] = setup_delta_p_conditions_gamma(p,gb);
    % all probablity distributions are now expressible as p + gamma_basis * x  
    %[lb,ub] = gamma_bounds(p,igb);
    if nargin < 5
        initial_value = zeros(size(gb,2),1); 
    end

%     if nargin < 5 && strcmp(algorithm,'interior-point')
%         options = optimoptions('linprog');
%         options.Algorithm = 'dual-simplex';%'interior-point';
%         [xnew,~,exitflag,~,~] = linprog(initial_value,condleq_lhs,condleq_rhs,[],[],[],[],[],options);
%         if exitflag == -2
%             disp('no feasible starting point found - revert to zero');
%             xnew = initial_value;
%         end
%     else
%         xnew = initial_value;
%     end
    global HISTORY;
    HISTORY.x = [];
    HISTORY.fval = [];
    xnew = initial_value;%(lb + ub)/2;
    f = @(x) gamma_objective_gradient(x,p,gb,y0,z0);
    options = optimoptions('fmincon');
    options.SpecifyObjectiveGradient = true;
    options.HessianFcn = @(x,~) hess_gamma(x,p,gb,igb,y0,z0);
    %options.Display = 'iter-detailed';
    options.OutputFcn = @outfun;
    %options.SubproblemAlgorithm = 'cg';
    %options.CheckGradients = true;
    %options.InitBarrierParam = 1;
    options.Algorithm = algorithm;%'sqp';%'active-set';
    options.HonorBounds = true;
    %options.PlotFcn = @optimplotfval;
    options.OptimalityTolerance = 1e-6;
    options.ConstraintTolerance = 1e-6;
    %options.StepTolerance = 1e-12;
    %options.UseParallel = true;
    %options.MaxFunctionEvaluations = 1000000;
    %options.MaxIterations = 10000;
    [x_orig,fval,exitflag,output,lambda,grad,hessian] = ...
        fmincon(f,xnew,condleq_lhs,condleq_rhs,[],[],[],[],[],options);
    if exitflag == -2 
        options.Algorithm = 'sqp';
        disp('WARNING! first algorithm did not find feasible point')
        [x_orig,fval,exitflag,output,lambda,grad,hessian] = ...
        fmincon(f,xnew,condleq_lhs,condleq_rhs, ...
                [],[],[],[],[],options);
    end
    x = p + reshape(gb * x_orig,size(p));
end

function stop = outfun(x,optimValues,state)
    global HISTORY;
    switch state
        case 'iter'
            HISTORY.fval = [HISTORY.fval, optimValues.fval];
            HISTORY.x = [HISTORY.x, x];
    end
    stop = false;
end
