function [x,fval,exitflag,output,lambda,grad,hessian,x_orig] = Min_MI_X_YZ_direct(p)
%OPTIMIZATION_DIRECT Summary of this function goes here
%   Detailed explanation goes here
    [cond_lhs,cond_rhs] = setup_delta_p_conditions_naive(p);
    % consistency check: marg_cond_lhs * p(:) == marg_cond_rhs
    options = optimoptions('fmincon');
    options.Algorithm =  'sqp'%'active-set';
    % options.MaxFunctionEvaluations = 1000000;
    % options.MaxIterations = 10000;
    [x_orig,fval,exitflag,output,lambda,grad,hessian] = ...
        fmincon(@MI_X_YZ,p(:),[],[],cond_lhs,cond_rhs,...
                zeros(size(p(:))),ones(size(p(:))),[],options);
    x = reshape(x_orig,size(p));
end

