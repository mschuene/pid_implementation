function [ h ] = hess_gamma(x,p,gb,igb,y,z)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

    % lambda can be ignored because we only have linear constraints
    [cardx,cardy,cardz] = size(p);
    n = size(gb,2);
    Q = reshape(p(:) + gb*x,size(p));
    
%    Q = Q + max(-min(Q(:),0));
%    Q = Q ./ sum(Q(:));
    
    Q_YZ = squeeze(sum(Q,1));
    eps = 0.001;
    h = (1/log(2)) * bsxfun(@(i,j)hij(i,j,igb,y,z,Q,Q_YZ,eps),1:n,(1:n)');
    h(isnan(h)) = 0; 
    %h(h > 100) = 100; h(h < -100) = -1000
end


function [h] = hij(i,j,igb,y,z,Q,Q_YZ,eps) % y z sind y0 und z0 x,yp,zp sind jeweils in c1 bzw c2
    
    c1 = igb(i,:);
    c2 = igb(j,:);
    
    delta_x = bsxfun(@eq,c1(1),c2(:,1));
    delta_yp = bsxfun(@eq,c1(2),c2(:,2));
    delta_zp = bsxfun(@eq,c1(3),c2(:,3));
    delta_xyp = (delta_x + delta_yp) == 2; 
    delta_xzp = (delta_x + delta_zp) == 2;
    delta_ypzp = (delta_yp + delta_zp) == 2;
    delta_xypzp = (delta_xyp + delta_zp) == 2;
    
    Q1 = Q(c1(1),y,z) + eps*(Q(c1(1),y,z) == 0);
    Q2 = Q(c1(1),c1(2),c1(3)) + eps*(Q(c1(1),c1(2),c1(3)) == 0);
    Q3 = Q(c1(1),c1(2),z) + eps*(Q(c1(1),c1(2),z) == 0);
    Q4 = Q(c1(1),y,c1(3)) + eps*(Q(c1(1),y,c1(3)) == 0);
    Q5 = Q_YZ(c1(2),z) + eps*(Q_YZ(c1(2),z) == 0);
    Q6 = Q_YZ(y,c1(3)) + eps*(Q_YZ(y,c1(3)) == 0);
    Q7 = Q_YZ(y,z) + eps*(Q_YZ(y,z) == 0);
    Q8 = Q_YZ(c1(2),c1(3)) + eps*(Q_YZ(c1(2),c1(3)) == 0);
    h = delta_x./Q1 + ...
        delta_xypzp./Q2 + ...
        delta_xyp./Q3 + ...
        delta_xzp./Q4 - ...
        delta_yp./Q5 - ...
        delta_zp./Q6 - ...
        1./Q7 - ...
        delta_ypzp./Q8;
    
    
%    h = 0   
%    if c1(1) == c2(1)
%        h = h + 1./Q(c1(1),y,z);
%        if c1(2) == c2(2)
%            h = h + 1./Q(c1(1),c1(2),z)
%            if c1(3) == c2(3)
%                h = h + 1./Q(c1(1),c1(2),c1(3))
%            end
%        end
%        if c1(3) == c2(3)
%            h = h + 1./Q(c1(1),y,c1(3));
%        end
%    end 
%    if c1(2) == c2(2)
%        h = h - 1./(Q_YZ(c1(2),z));
%        if c1(3) == c2(3)
%            h = h - 1./Q_YZ(c1(2),c1(3));
%        end
%    end
%    if c1(3) == c2(3)
%        h = h - 1./Q_YZ(y,c1(3));
%    end
%    h = h - 1./Q_YZ(y,z);
%    disp('size of h');
%    h
end