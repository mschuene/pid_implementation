function [ gamma_grad ] = calculate_gamma_gradient(Q,y0,z0,cardx,cardy,cardz,clip,eps)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 8
        eps = 1e-8;
    end
    if nargin < 7
        clip = 100000; % clip gradient to this size in max norm
    end
    if nargin < 4
        cardx = 2; cardy = 2; cardz = 2;
    end
    if nargin < 2
        y0 = 1; 
        z0 = 1;
    end
    Q = reshape(Q,[cardx,cardy,cardz]);
    Q_YZ = squeeze(sum(Q,1));
    gamma_grad = zeros(cardx*(cardy - 1)*(cardz - 1),1);
    ggrad = @(x,y,z,eps) log2(Q(x,y0,z0) + eps) + log2(Q(x,y,z) + eps) + log2(Q_YZ(y,z0) + eps) + ...
        log2(Q_YZ(y0,z) + eps) - log2(Q(x,y,z0) + eps) - log2(Q(x,y0,z) + eps) - ...
        log2(Q_YZ(y0,z0) + eps) - log2(Q_YZ(y,z) + eps);
    i = 0;
    for x=1:cardx
        for y=1:cardy
            if y ~= y0
                for z=1:cardz
                    if z~= z0
                        i = i + 1;
                        gg = ggrad(x,y,z,0);
                        if isnan(gg)
                            gg = ggrad(x,y,z,eps);
                        end
                        %if ~(isfinite(gg))
                            %gg = ggrad(x,y,z,eps);
                        %end
                        if abs(gg) > clip
                            gg = clip*sign(gg);
                        end
                        gamma_grad(i) = gg;
                    end
                end
            end
        end
    end
    
                            

end

