function [ condleq_lhs,condleq_rhs ] = setup_delta_p_conditions_gamma(p,gamma_basis)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

    % conditions all elements >=  zero and <= 1
    condleq_lhs = [-gamma_basis;gamma_basis];
    condleq_rhs = [p(:);1 - p(:)];

end

