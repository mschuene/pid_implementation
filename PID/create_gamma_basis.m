function [ gamma_basis,igb] = create_gamma_basis(y0,z0,cardx,cardy,cardz)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    if (nargin < 3)
        cardx = 2; cardy = 2; cardz = 2;
    end

    gamma_basis = zeros(cardx*cardy*cardz,cardx*(cardy - 1)*(cardz -1));
    igb = zeros(cardx*(cardy - 1)*(cardz - 1),3); %save what xyz direction is at what position
    i = 0;
    for x = 1:cardx
        for y = 1:cardy
            if(y ~= y0)
                for z = 1:cardz
                    if z ~= z0
                        i = i + 1;
                        gd = gamma_direction(x,y0,y,z0,z,cardx,cardy,cardz);
                        gamma_basis(:,i) = gd(:);
                        igb(i,:) = [x,y,z];
                    end
                end
            end
        end
    end
end

