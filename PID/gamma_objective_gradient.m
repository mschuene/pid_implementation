function [ value,grad ] = gamma_objective_gradient(x,p,gamma_basis,y0,z0)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here
    if nargin < 4
        y0 = 1; z0 = 1;
    end
    [cardx,cardy,cardz] = size(p);
    Q = p(:) + gamma_basis * x;
    Qneg = min(0,Q);
    if sum(abs(Qneg)) > 1e-6
%         Q = Q + max(-min(Q(:),0));
%         Q = Q ./ sum(Q(:));
%         value = real(MI_X_YZ(Q,cardx,cardy,cardz));
%         grad = real(calculate_gamma_gradient(Q,y0,z0,cardx,cardy,cardz));
        value = Inf;
        grad = ones(size(gamma_basis,2),1).*Inf;
    else
        Q = Q + max(-min(Q(:),0));
        Q = Q ./ sum(Q(:));
        value = real(MI_X_YZ(Q,cardx,cardy,cardz));
        if value < 0
            value = Inf;
        end
        if nargout > 1
            grad = real(calculate_gamma_gradient(Q,y0,z0,cardx,cardy,cardz));
            grad(isnan(grad)) = 0;
        end
    end

end

